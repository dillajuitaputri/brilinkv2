package common

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable


import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.configuration.RunConfiguration

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver as AppiumDriver
import io.appium.java_client.MobileElement

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class screenshot {
	def takeScreenshot(String feature){
		//		Date today = new Date()
		//
		//		String todaysDate = today.format('MM_dd_yy')
		//
		//		String nowTime = today.format('hh_mm_ss')
		//
		//		Mobile.takeScreenshotAsCheckpoint('Screenshot\\screenshot_' + todaysDate + '-' + nowTime + '.png', FailureHandling.STOP_ON_FAILURE)
		//

		String filePath = RunConfiguration.getProjectDir()

		Date today = new Date()

		String todaysDate = today.format('MM_dd_yyyy')

		String nowTime = today.format('hh_mm_ss')

		String PATH = filePath + '/Screenshot/';

		String directoryName = PATH.concat(todaysDate);

		File directory = new File(directoryName);

		if (! directory.exists()){
			directory.mkdir();
			// If you require it to make the entire directory path including parents,
			// use directory.mkdirs(); here instead.
		}

		String directoryFeature = directoryName +'/'+ feature;

		File directory2 = new File(directoryFeature);

		if (! directory2.exists()){
			directory2.mkdir();
			// If you require it to make the entire directory path including parents,
			// use directory.mkdirs(); here instead.
		}

		//String imageDir = Mobile.takeScreenshotAsCheckpoint((((filePath + '/Screenshot/iOS2/Katalon Picture_' + todaysDate) + '-')+ nowTime) + '.png', FailureHandling.STOP_ON_FAILURE)

		//Mobile.comment(imageDir)

		//File pathFile = new File(imageDir);

		AppiumDriver<?> driver = MobileDriverFactory.getDriver()

		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

		try {
			BufferedImage imageSrc = ImageIO.read(scrFile);
			Image resultingImage = imageSrc.getScaledInstance(621,1278 , Image.SCALE_SMOOTH);
			// save the resize image aka thumbnail
			ImageIO.write(
					convertToBufferedImage(resultingImage),
					"png",
					new File(directoryFeature + '/Katalon Picture_' + todaysDate +'-'+nowTime+'.png'));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done");
	}

	//convert Image to BufferedImage
	public static BufferedImage convertToBufferedImage(Image img) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}
		// Create a buffered image with transparency
		BufferedImage bi = new BufferedImage(
				img.getWidth(null), img.getHeight(null),
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics2D = bi.createGraphics();
		graphics2D.drawImage(img, 0, 0, null);
		graphics2D.dispose();
		return bi;
	}
}
