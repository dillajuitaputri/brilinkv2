package common

import java.sql.*
import com.kms.katalon.core.annotation.Keyword
//import com.kms.katalon.core.annotation.Keyword


public class db {
	private static Connection connection = null;

	/**
	 * Open and return a connection to database
	 * @param dataFile absolute file path
	 * @return an instance of java.sql.Connection
	 */
	//Establishing a connection to the DataBase
	@Keyword

	def connectDB(String url, String dbname, String port, String username, String password) {
		//Load driver class for your specific database type
		String conn = "jdbc:mysql://" + url + ":" + port + "/" + dbname + "?serverTimezone=UTC"
		//Class.forName("org.sqlite.JDBC")
		//String connectionString = "jdbc:sqlite:" + dataFile
		if (connection != null && !connection.isClosed()) {
			connection.close()
		}
		connection = DriverManager.getConnection(conn, username, password)
		return connection
	}

	/**
	 * execute a SQL query on database
	 * @param queryString SQL query string
	 * @return a reference to returned data collection, an instance of java.sql.ResultSet
	 */
	//Executing the constructed Query and Saving results in resultset
	@Keyword

	def executeQuery(String queryString) {
		Statement stm = connection.createStatement()
		ResultSet rs = stm.executeQuery(queryString)
		return rs
	}

	//execute update a SQL query on database
	@Keyword

	def executeUpdate(String queryString) {
		Statement stm = connection.createStatement()
		stm.executeUpdate(queryString)
	}

	@Keyword

	def updateRefNum(){
		executeUpdate("UPDATE tbl_rrn SET rrn = LAST_INSERT_ID(rrn+1) WHERE id = 1")
		ResultSet refnum = executeQuery("SELECT * FROM tbl_rrn WHERE id = 1")
		refnum.next()
		String updatedRefnum = refnum.getString('rrn')
		return updatedRefnum
	}


	//Closing the connection
	@Keyword

	def closeDatabaseConnection() {
		if (connection != null && !connection.isClosed()) {
			connection.close()
		}
		connection = null
	}

	/**
	 * Execute non-query (usually INSERT/UPDATE/DELETE/COUNT/SUM...) on database
	 * @param queryString a SQL statement
	 * @return single value result of SQL statement
	 */
	@Keyword

	def execute(String queryString) {
		Statement stm = connection.createStatement()
		boolean result = stm.execute(queryString)
		return result
	}
}