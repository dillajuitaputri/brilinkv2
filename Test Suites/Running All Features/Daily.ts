<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Daily</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d92954c5-09b4-42f2-b5ab-46048cac24ea</testSuiteGuid>
   <testCaseLink>
      <guid>b8438c32-8773-4914-a7e4-03614b9f62e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Feature Existing/BAZNAS/Pembayaran/TC101 - BAZNAS Payment with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7d2bb5c-6a4a-4d6e-8f74-74d7e1acd757</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Feature Existing/Tiket KAI/TC101 - Transaction Tiket KAI with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15b651ec-00e6-4446-b13e-682a2079d559</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Feature Existing/Tokopedia/TC101 - Transaction Tokopedia with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77e216dc-3f50-4bdb-9ac5-4d25603ec002</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Feature Existing/Top Up LinkAja/TC101 - Transaction Top Up LinkAja with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58c2036d-c4b3-4094-9076-f0ce7e7f59d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/Setor Pinjaman/TC101 - Transaction Setor Pinjaman with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8f2988b-8806-487f-b87f-fd257c000d8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/Setor Simpanan/TC101 - Transaction Setor Simpanan with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d08f886e-b0b1-4a6f-8c1f-6496220950f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/Telepon Pascabayar/TC101 - Transaction Telepon Pascabayar with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dfc4066e-3096-4bf7-9f9a-6973d45804ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/Top Up Gopay/Customer/TC101 - Transaction Top Up Gopay Cutomer with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>edde2eeb-7049-451c-850a-11664d0c27c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/Top Up Gopay/Driver/TC101 - Transaction Top Up Gopay Driver with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b232ab6-7632-4b53-a055-878d514a9b15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/Top Up Gopay/Merchant/TC101 - Transaction Top Up Gopay Merchant with Data Valid</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
