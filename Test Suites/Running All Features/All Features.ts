<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>This is Test Suites for running all features BRILink</description>
   <name>All Features</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>4cd9bc26-04c4-4589-b10c-200967908085</testSuiteGuid>
   <testCaseLink>
      <guid>0c3b5a17-ac35-4384-ae54-a0ef4929d9be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Feature Existing/Top Up LinkAja/TC101 - Transaction Top Up LinkAja with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11603aa3-9bb4-411b-bddc-efe878d268a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Feature Existing/Tokopedia/TC101 - Transaction Tokopedia with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25704ef9-5a91-44e4-a3fe-a9a18981a945</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Feature Existing/Tiket Pesawat/TC101 - Transaction Tiket Pesawat with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59578f8e-3f16-4f5d-9317-1132ae38e037</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Feature Existing/Tiket KAI/TC101 - Transaction Tiket KAI with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1bc396c-c8f8-4ef5-ba8c-2e92d481a30c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Feature Existing/Dana Talangan/TC101 - Open Menu Dana Talangan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2fe1f9b4-9031-4a5e-aa96-e8932a0c29a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Feature Existing/BPJS Kesehatan/TC101 - Transaction BPJS KS with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebadfa37-b65b-4940-b524-ef9e6a4b3724</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Feature Existing/BAZNAS/Register/TC101 - BAZNAS Register with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87910fa1-3d5e-47d7-a703-9e8a4a48d92c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Feature Existing/BAZNAS/Pembayaran/TC101 - BAZNAS Payment with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48ff337b-f0e3-494b-abb4-d000293c3b84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/PLN/Cetak Ulang Token/TC101 - Cetak Ulang Token with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6325ae2b-aa71-435b-bb1e-a27b9f583ad7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/PLN/Non Taglis/TC101 - Transaction PLN Non Taglis with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f357824-40ab-470e-bf57-90155dbdbd2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/PLN/Pascabayar/TC101 - Transaction PLN Pascabayar with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77ce46dc-235a-4c1a-a964-9b66401960c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/PLN/Prabayar/TC101 - Transaction PLN Prabayar with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c01c5583-9c66-4efb-9932-f8a249b3504c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/Setor Pinjaman/TC101 - Transaction Setor Pinjaman with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>27b74ba6-d306-428f-b06e-e9818e292440</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/Setor Simpanan/TC101 - Transaction Setor Simpanan with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00918454-aed0-4531-8492-e0ee67a67ef6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/Telepon Pascabayar/TC101 - Transaction Telepon Pascabayar with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e16ff831-0c7f-46d1-8d3b-94955325fd3f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/Top Up Gopay/Customer/TC101 - Transaction Top Up Gopay Cutomer with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3a4dc86-41ba-4703-9217-0d266beb5dea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/Top Up Gopay/Driver/TC101 - Transaction Top Up Gopay Driver with Data Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebeedfb2-b4fc-48e7-b897-8c78231563a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1DEMO FOR TEST SUITES/Main Feature/Top Up Gopay/Merchant/TC101 - Transaction Top Up Gopay Merchant with Data Valid</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
