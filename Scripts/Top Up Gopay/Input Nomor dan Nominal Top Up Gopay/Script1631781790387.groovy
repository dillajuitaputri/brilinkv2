import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.keyword.excel.ExcelKeywords

def workBook = ExcelKeywords.getWorkbook("Datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Top Up Gopay")

String noHP = ExcelKeywords.getCellValueByAddress(sheet1, 'A'+GlobalVariable.rowNumber)
String nominal = ExcelKeywords.getCellValueByAddress(sheet1, 'B'+GlobalVariable.rowNumber)

//set text
Mobile.setText(findTestObject('Object Repository/Top Up Gopay/EditText - Nomor Telepon'), noHP.toString(), 0)
Mobile.setText(findTestObject('Object Repository/Top Up Gopay/EditText - Nominal'), nominal.toString(), 0)

Mobile.delay(2)
//ss
//CustomKeywords.'common.screenshot.takeScreenshot'()

//tap button kirim
Mobile.tap(findTestObject('Object Repository/Tiket KAI/Button - KIRIM'), 0)

Mobile.delay(3)
//call testcase notifikasi
Mobile.callTestCase(findTestCase('Test Cases/Chuck/Notifikasi Chuck'), null)
