import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import groovy.transform.EqualsAndHashCode

//chs??
Mobile.tap(findTestObject('Object Repository/Catatan Aktivitas/three line'), 0)
Mobile.delay(3)
Mobile.tap(findTestObject('Object Repository/Catatan Aktivitas/android.widget.TextView - Catatan Aktivitas'), 0)
Mobile.delay(3)
Mobile.tap(findTestObject('Object Repository/Briva/layout'), 0)

//tgl trx
//GlobalVariable.caDate = Mobile.getText(findTestObject('Object Repository/Top Up LinkAja/master/csv/catatan aktivitas/date'), 0)
//Mobile.comment(GlobalVariable.caDate)

//nmr referensi
GlobalVariable.caRefnum = Mobile.getText(findTestObject('Object Repository/Top Up LinkAja/master/csv/catatan aktivitas/noref'), 0)
Mobile.comment(GlobalVariable.caRefnum)

//jumlah pembayaran
GlobalVariable.caAmount = Mobile.getText(findTestObject('Object Repository/Top Up LinkAja/master/csv/catatan aktivitas/nominal'), 0)
Mobile.comment(GlobalVariable.caAmount)

//fee
GlobalVariable.caFee = Mobile.getText(findTestObject('Object Repository/Top Up LinkAja/master/csv/catatan aktivitas/fee'), 0)
Mobile.comment(GlobalVariable.caFee)

//total
GlobalVariable.caTotal = Mobile.getText(findTestObject('Object Repository/Top Up LinkAja/master/csv/catatan aktivitas/total'), 0)
Mobile.comment(GlobalVariable.caTotal)

//status
GlobalVariable.caStatus = Mobile.getText(findTestObject('Object Repository/Top Up LinkAja/master/csv/catatan aktivitas/status'), 0)
Mobile.comment(GlobalVariable.caStatus)

//compare
//if (GlobalVariable.caDate == GlobalVariable.trxDate) {
//	Mobile.comment('iya bener date nya')
//} else {
//	Mobile.comment(GlobalVariable.caDate)
//	Mobile.comment(GlobalVariable.trxDate)
//	throw new Exception("failed")
//	
//}

 if (GlobalVariable.caRefnum == GlobalVariable.trxRefnum) {
	Mobile.comment('noref correct')
 } else {
	 Mobile.comment(GlobalVariable.caRefnum)
	 Mobile.comment(GlobalVariable.trxRefnum)
	 throw new Exception("failed")
 }
  
 if (GlobalVariable.caAmount == GlobalVariable.trxAmount) {
	Mobile.comment('amount correct')
} else {
	Mobile.comment(GlobalVariable.caAmount)
	Mobile.comment(GlobalVariable.trxAmount)
	 throw new Exception("failed")
}

if (GlobalVariable.caFee == GlobalVariable.trxFee) {
	Mobile.comment('fee correct')
} else {
	Mobile.comment(GlobalVariable.caFee)
	Mobile.comment(GlobalVariable.trxFee)
	throw new Exception("failed")
}

if (GlobalVariable.caTotal == GlobalVariable.trxTotal) {
	Mobile.comment('total correct')
} else {
	Mobile.comment(GlobalVariable.caTotal)
	Mobile.comment(GlobalVariable.trxTotal)
	 throw new Exception("failed")
}



//export csv
Scanner sc = new Scanner(new File("customer.csv"))
String strNumber
while(sc.hasNext()) {
	String filePerLine = sc.nextLine()
	String[] data = filePerLine.split(",")
	strNumber = data[0].trim();
}
int number = 1
FileWriter writer = new FileWriter("customer.csv",true);
if (strNumber != null) {
	writer.write("\n")
	number = Integer.parseInt(strNumber)
	number = number + 1
}
Mobile.comment(feature.toString())
writer.write(number + "," +feature.toString() +","+","+GlobalVariable.caRefnum+","+GlobalVariable.caAmount+","+GlobalVariable.caFee+","+GlobalVariable.caTotal+","+GlobalVariable.caStatus);
sc.close()
writer.close();
