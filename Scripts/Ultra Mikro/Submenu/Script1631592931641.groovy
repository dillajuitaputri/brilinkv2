import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

def workBook = ExcelKeywords.getWorkbook("Datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Ultra Mikro")
def sheet2 = ExcelKeywords.getExcelSheet(workBook,"Tarik Tunai LinkAja")
def sheet3 = ExcelKeywords.getExcelSheet(workBook,"Pegadaian")

String token = ExcelKeywords.getCellValueByAddress(sheet2, 'A'+GlobalVariable.rowNumber)
String nominal = ExcelKeywords.getCellValueByAddress(sheet2, 'B'+GlobalVariable.rowNumber)
String subMenu = ExcelKeywords.getCellValueByAddress(sheet1, 'A'+GlobalVariable.rowNumber)
String halaman = ExcelKeywords.getCellValueByAddress(sheet3, 'A'+GlobalVariable.rowNumber)
String jenisTransaksi = ExcelKeywords.getCellValueByAddress(sheet3, 'B'+GlobalVariable.rowNumber)
String noRekTabEmas = ExcelKeywords.getCellValueByAddress(sheet3, 'C'+GlobalVariable.rowNumber)
String noRekPinjaman = ExcelKeywords.getCellValueByAddress(sheet3, 'D'+GlobalVariable.rowNumber)
String nominalPembayaran = ExcelKeywords.getCellValueByAddress(sheet3, 'E'+GlobalVariable.rowNumber)
String nominalPinjaman = ExcelKeywords.getCellValueByAddress(sheet3, 'F'+GlobalVariable.rowNumber)
String nama = ExcelKeywords.getCellValueByAddress(sheet3, 'G'+GlobalVariable.rowNumber)
String noHp = ExcelKeywords.getCellValueByAddress(sheet3, 'H'+GlobalVariable.rowNumber)
String noKTP = ExcelKeywords.getCellValueByAddress(sheet3, 'I'+GlobalVariable.rowNumber)
String tglLahir = ExcelKeywords.getCellValueByAddress(sheet3, 'J'+GlobalVariable.rowNumber)
String tipeKendaraan = ExcelKeywords.getCellValueByAddress(sheet3, 'K'+GlobalVariable.rowNumber)
String kepemilikan = ExcelKeywords.getCellValueByAddress(sheet3, 'L'+GlobalVariable.rowNumber)
String keterangan = ExcelKeywords.getCellValueByAddress(sheet3, 'M'+GlobalVariable.rowNumber)
String noPolisi = ExcelKeywords.getCellValueByAddress(sheet3, 'N'+GlobalVariable.rowNumber)
String tahunPerakitan = ExcelKeywords.getCellValueByAddress(sheet3, 'O'+GlobalVariable.rowNumber)
String tokenTarikTunai = ExcelKeywords.getCellValueByAddress(sheet3, 'P'+GlobalVariable.rowNumber)
String noRek = ExcelKeywords.getCellValueByAddress(sheet3, 'Q'+GlobalVariable.rowNumber)

//set text
switch (subMenu) {
			case 'linkaja':
//				CustomKeywords.'screenshot.capture.Screenshot'()

				Mobile.tap(findTestObject('Ultra Mikro/Submenu/TextView - Tarik Tunai LinkAja'), 0)

//				GlobalVariable.typeGopay = 'Customer'
				
				Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Tarik Tunai Link Aja/EditText - Tap Token Link Aja'), 0)
				
				Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Tarik Tunai Link Aja/EditText - Input Token Manual'), token.toString(), 0)
				
				Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Tarik Tunai Link Aja/Button - KIRIM input token'), 0)
				
				Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Tarik Tunai Link Aja/EditText - Nominal'), nominal.toString(), 0)
				
				Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Tarik Tunai Link Aja/Button - KIRIM'), 0)
				break
				
			case 'pegadaian':
//				CustomKeywords.'screenshot.capture.Screenshot'()

				Mobile.tap(findTestObject('Ultra Mikro/Submenu/TextView - Pegadaian'), 0)
//				GlobalVariable.typeGopay = 'Driver'
				
		switch (halaman) {
			case 'pembayaran':
				Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Pembayaran/TextView - PEMBAYARAN'), 0)
				
				Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Pembayaran/EditText - Jenis Transaksi'), 0)
				
				switch (jenisTransaksi) {
					case 'topuptabunganemas':
					Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Pembayaran/TextView - Topup Tabungan Emas'), 0)
					
					Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Pembayaran/EditText - Nomor Rekening Tabungan Emas'), noRekTabEmas.toString(), 0)
					
					Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Pembayaran/EditText - Nominal'), nominal.toString(), 0)
					
					break
					
					case 'pembayaranpinjaman':
					Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Pembayaran/TextView - Pembayaran Pinjaman'), 0)
					
					Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Pembayaran/EditText - Nomor Rekening Pinjaman'), noRekPinjaman.toString(), 0)
					
					break
					default:
					break }
				
				Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Pembayaran/Button - KIRIM'), 0)
				break
				
				case 'referralPinjaman':
				
				device_height = Mobile.getDeviceHeight()
				device_width = Mobile.getDeviceWidth()
				
				int startX = device_width/2
				int endX = startX
				int startY = device_height * 0.70
				int endY = device_height * 0.20 
				
					Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/TextView - REFERRAL PINJAMAN'), 0)
				
					Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/EditText - Nominal Pinjaman'), nominalPinjaman.toString(), 0)
				
					Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/EditText - Nama Nasabah'), nama.toString(), 0)
				
					Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/EditText - Nomor Handphone'), noHp.toString(), 0)
				
					Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/EditText - Nomor KTP'), noKTP.toString(), 0)
				
					Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/EditText - Tanggal Lahir'), tglLahir.toString(), 0)
				
					Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/EditText - Tipe Kendaraan'), 0)
						switch (tipeKendaraan) {
							case 'mobil':
							Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/TextView - Kendaraan Mobil'), 0)
							break
							case 'motor':
							Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/TextView - Kendaraan Sepeda Motor'), 0)
							break
							default:
							break }
					
						Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/EditText - Kepemilikan'), 0)
							switch (kepemilikan) {
							case 'miliknasabah':
								Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/TextView - Kendaraan an Nasabah'), 0)
							break
							case 'bukanmiliknasabah':
								Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/TextView - Kendaraan bukan an Nasabah'), 0)
							break
							case 'miliksuami-istri':
								Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/Text View - Kendaraan Milik Suami Istri Nasabah'), 0)
							break
							case 'milikkerabat':
								Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/TextView - Kendaraan Milik Kerabat'), 0)
							break
							case 'barumiliknasabah':
								Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/TextView - Kendaraan Baru Atas Nama Nasabah'), 0)
							break
							case 'bekasmiliknasabah':
								Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/TextView - Kendaraan Bekas Atas Nama Nasabah'), 0)
							break
							default:
							break }
							Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/EditText - Keterangan'), keterangan.toString(), 0)
				
							Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/EditText - No. Polisi'), noPolisi.toString(), 0)
							
							Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/EditText - Tahun Perakitan'), tahunPerakitan.toString(), 0)
							
							Mobile.swipe(startX, startY, endX, endY)
 
							Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/Button - KIRIM'), 0)
							break
				default:
				break }

				case 'tarikTunai':
				Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Tarik Tunai/TextView - TARIK TUNAI'), 0)
	
				Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Tarik Tunai/EditText - Token Tarik Tunai Pegadaian'), tokenTarikTunai.toString(), 0)
				
				Mobile.setText(findTestObject('Object Repository/Ultra Mikro/Tarik Tunai/EditText - Nomor Rekening'), noRek.toString(), 0)
				
				Mobile.tap(findTestObject('Object Repository/Ultra Mikro/Pegadaian/Referral Pinjaman/Button - KIRIM'), 0)
				break
				default:
				break }
				//ss
CustomKeywords.'common.screenshot.takeScreenshot'()
