import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.keyword.excel.ExcelKeywords

def workBook = ExcelKeywords.getWorkbook("Datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"PLN")

String nominalTrxPLN = ExcelKeywords.getCellValueByAddress(sheet1, 'C'+GlobalVariable.rowNumber)

//
//Tap Dropdown Jenis Cicilan
Mobile.tap(findTestObject('Object Repository/PLN/Spinner'), 0)

//
Mobile.delay(3)

//ss

nominal = nominalTrxPLN.toString()

switch (nominal) {
	case 'IDR 20,000':
		Mobile.tap(findTestObject('Object Repository/PLN/master/dropdown/prabayar nominal/android.widget.TextView - IDR 20,000'), 0)
		break
	case 'IDR 50,000':
		Mobile.tap(findTestObject('Object Repository/PLN/master/dropdown/prabayar nominal/android.widget.TextView - IDR 50,000'), 0)
		break
	case 'IDR 100,000':
		Mobile.tap(findTestObject('Object Repository/PLN/master/dropdown/prabayar nominal/android.widget.TextView - IDR 100,000'), 0)
		break
	case 'IDR 200,000':
		Mobile.tap(findTestObject('Object Repository/PLN/master/dropdown/prabayar nominal/android.widget.TextView - IDR 200,000'), 0)
		break
	case 'IDR 500,000':
		Mobile.tap(findTestObject('Object Repository/PLN/master/dropdown/prabayar nominal/android.widget.TextView - IDR 500,000'), 0)
		break
	case 'IDR 1,000,000':
		Mobile.tap(findTestObject('Object Repository/PLN/master/dropdown/prabayar nominal/android.widget.TextView - IDR 1,000,000'), 0)
		break
	case 'IDR 5,000,000':
		Mobile.tap(findTestObject('Object Repository/PLN/master/dropdown/prabayar nominal/android.widget.TextView - IDR 5,000,000'), 0)
		break
	case 'IDR 10,000,000':
		Mobile.tap(findTestObject('Object Repository/PLN/master/dropdown/prabayar nominal/android.widget.TextView - IDR 10,000,000'), 0)
		break
	case 'IDR 50,000,000':
		Mobile.tap(findTestObject('Object Repository/PLN/master/dropdown/prabayar nominal/android.widget.TextView - IDR 50,000,000'), 0)
		break
	case 'TOKEN AVAIL1 - IDR 100,000':
		Mobile.tap(findTestObject('Object Repository/PLN/master/dropdown/prabayar nominal/android.widget.TextView - TOKEN AVAIL1 - IDR 100,000'), 0)
		break
	case 'TOKEN AVAIL2 - IDR 500,000':
		Mobile.tap(findTestObject('Object Repository/PLN/master/dropdown/prabayar nominal/android.widget.TextView - TOKEN AVAIL2 - IDR 500,000'), 0)
		break
	
}

//ss
CustomKeywords.'common.screenshot.takeScreenshot'()

//tap button kirim
Mobile.tap(findTestObject('Object Repository/PLN/Button - KIRIM - V2'), 0)