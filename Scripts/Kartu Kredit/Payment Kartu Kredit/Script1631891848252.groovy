import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

def workBook = ExcelKeywords.getWorkbook("Datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Common")

String password = ExcelKeywords.getCellValueByAddress(sheet1, 'C2')

//set text
//Mobile.setText(findTestObject('Object Repository/Common/EditText - Password'), password.toString(), 0)
Mobile.setText(findTestObject('Object Repository/Kartu Kredit/android.widget.EditText - Password'), password, 0)

//tap button bayar
Mobile.tap(findTestObject('Object Repository/Kartu Kredit/android.widget.Button - BAYAR'), 0)

Mobile.delay(3)

Mobile.callTestCase(findTestCase('Test Cases/Chuck/Notifikasi Chuck'), null)

Mobile.delay(3)

Mobile.pressHome()
Mobile.tap(findTestObject('Object Repository/Chuck/android.widget.TextView - (Dev)Brilink Mobile'), 0)

Mobile.tap(findTestObject('Object Repository/Catatan Aktivitas/three line'), 0)
Mobile.delay(3)
Mobile.tap(findTestObject('Object Repository/Catatan Aktivitas/android.widget.TextView - Catatan Aktivitas'), 0)
Mobile.delay(3)
Mobile.tap(findTestObject('Object Repository/Briva/layout'), 0)
