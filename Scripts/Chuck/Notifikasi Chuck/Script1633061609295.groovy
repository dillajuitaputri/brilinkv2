import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//String chuck = GlobalVariable.chuck
//
//if (chuck.toLowerCase() == "yes") {
//	Mobile.openNotifications()
//	Mobile.tap(findTestObject('Object Repository/Chuck/view all activity'), 0)
//	Mobile.tap(findTestObject('Object Repository/Chuck/LinearLayout'), 0)
//	Mobile.tap(findTestObject('Object Repository/Chuck/Chuck Terbaru'), 0)
//	Mobile.delay(3)
//	Mobile.tap(findTestObject('Object Repository/Chuck/android.widget.TextView - RESPONSE'), 0)
//	Mobile.delay(5)
//}
//else {
//	Mobile.delay(2)
//}
//

String chuck = GlobalVariable.chuck
String chuckSamsung = GlobalVariable.chuckSamsungNote20

if (chuck.toLowerCase() == "yes") {
		Mobile.openNotifications()
		Mobile.tap(findTestObject('Object Repository/Chuck/view all activity'), 0)
		Mobile.tap(findTestObject('Object Repository/Chuck/LinearLayout'), 0)
		Mobile.tap(findTestObject('Object Repository/Chuck/Chuck Terbaru'), 0)
		Mobile.delay(3)
		Mobile.tap(findTestObject('Object Repository/Chuck/android.widget.TextView - RESPONSE'), 0)
		Mobile.delay(5)
		Mobile.delay(3)
		Mobile.pressHome()
		//click icon brilink
		Mobile.tap(findTestObject('Object Repository/Chuck/android.widget.TextView - (Dev)Brilink Mobile'), 0)
} else if (chuckSamsung.toLowerCase() == "yes") {
	Mobile.openNotifications()
	//click arrow
	Mobile.tap(findTestObject('Object Repository/Chuck/Chuck Samsung Note 20/Arrow Icon'), 0)
	//click list recording http activity
	Mobile.tap(findTestObject('Object Repository/Chuck/Chuck Samsung Note 20/List Recording'), 0)
	//click list yang paling atas
	Mobile.tap(findTestObject('Object Repository/Chuck/Chuck Samsung Note 20/List Teratas'), 0)
	//click response
	Mobile.tap(findTestObject('Object Repository/Chuck/Chuck Samsung Note 20/android.widget.TextView - RESPONSE'), 0)
	Mobile.delay(5)
	Mobile.delay(3)
	Mobile.pressHome()
	//open app
	Mobile.startExistingApplication('id.co.bri.brilinkmobile')
} else {
	Mobile.delay(2)
}