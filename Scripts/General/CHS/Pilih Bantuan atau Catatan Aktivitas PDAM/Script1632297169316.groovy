import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.scrollToText('sukses')
if (Mobile.verifyElementExist(findTestObject('Object Repository/Catatan Aktivitas/Sukses'), 3, FailureHandling.OPTIONAL) == true) {
	Mobile.tap(findTestObject('Object Repository/Catatan Aktivitas/three line'), 0)
	Mobile.delay(3)
	Mobile.tap(findTestObject('Object Repository/Catatan Aktivitas/android.widget.TextView - Catatan Aktivitas'), 0)
	Mobile.delay(3)
	Mobile.tap(findTestObject('Object Repository/Catatan Aktivitas/android.widget.Spinner'), 0)
	Mobile.delay(3)
	Mobile.callTestCase(findTestCase('Test Cases/Catatan Aktivasi/Fitur PDAM'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	else {
	Mobile.verifyElementExist(findTestObject('Object Repository/CHS/android.widget.TextView - Sedang Diproses'),0)
	Mobile.callTestCase(findTestCase('Test Cases/CHS/Bantuan'), 0)
	}