import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//tgl trx
//GlobalVariable.trxDate = Mobile.getText(findTestObject('Object Repository/BAZNAS/master/csv/pembayaran/trx berhasil/date'), 0)
//Mobile.comment(GlobalVariable.trxDate)

//nmr referensi
GlobalVariable.trxRefnum = Mobile.getText(findTestObject('Object Repository/BAZNAS/master/csv/pembayaran/trx berhasil/noref'), 0)
Mobile.comment(GlobalVariable.trxRefnum)

//jumlah pembayaran
GlobalVariable.trxAmount = Mobile.getText(findTestObject('Object Repository/BAZNAS/master/csv/pembayaran/trx berhasil/nominal'), 0)
Mobile.comment(GlobalVariable.trxAmount)

//fee
GlobalVariable.trxFee = Mobile.getText(findTestObject('Object Repository/BAZNAS/master/csv/pembayaran/trx berhasil/fee'), 0)
Mobile.comment(GlobalVariable.trxFee)

//total
GlobalVariable.trxTotal = Mobile.getText(findTestObject('Object Repository/BAZNAS/master/csv/pembayaran/trx berhasil/total'), 0)
Mobile.comment(GlobalVariable.trxTotal)

