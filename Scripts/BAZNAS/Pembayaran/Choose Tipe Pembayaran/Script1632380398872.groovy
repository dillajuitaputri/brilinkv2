import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.keyword.excel.ExcelKeywords

def workBook = ExcelKeywords.getWorkbook("Datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"BAZNAS")

String tipePembayaran = ExcelKeywords.getCellValueByAddress(sheet1, 'C'+GlobalVariable.rowNumber)

//
//Tap Dropdown pembayaran
Mobile.tap(findTestObject('Object Repository/BAZNAS/Pembayaran/EditText - ZAKAT PROFESI'), 0)

//
Mobile.delay(3)

//ss

pembayaran = tipePembayaran.toString()

switch (pembayaran) {
	case 'Infak':
		Mobile.tap(findTestObject('Object Repository/BAZNAS/Pembayaran/TextView - INFAK'), 0)
		break
	case 'Kurban':
		Mobile.tap(findTestObject('Object Repository/BAZNAS/Pembayaran/TextView - KURBAN'), 0)
		break
	case 'Zakat Maal':
		Mobile.tap(findTestObject('Object Repository/BAZNAS/Pembayaran/TextView - ZAKAT MAAL'), 0)
		break
	case 'Zakat Profesi':
		Mobile.tap(findTestObject('Object Repository/BAZNAS/Pembayaran/TextView - ZAKAT PROFESI'), 0)
		break
}

//ss
CustomKeywords.'common.screenshot.takeScreenshot'()
