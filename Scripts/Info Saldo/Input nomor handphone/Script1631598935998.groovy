import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords


Mobile.tap(findTestObject('Object Repository/Info Saldo/android.widget.TextView - 0813xxxxx206 - SMS NOTIFIKASI (1)'), 0)

Mobile.tap(findTestObject('Object Repository/Info Saldo/Button - KIRIM OTP'), 0)

//ss
CustomKeywords.'common.screenshot.takeScreenshot'()

//set text
//Mobile.setText(findTestObject('Object Repository/Info Saldo/EditText - Kode OTP'), kodeOTP.toString(), 0)

Mobile.startExistingApplication('com.android.chrome')

Mobile.tap(findTestObject('Object Repository/Chrome/android.widget.TextView - Get OTP Brilink Mobile'), 0)

Mobile.tap(findTestObject('Object Repository/Chrome/android.widget.EditText'), 0)
String name = 'brilinkmo008'
Mobile.setText(findTestObject('Object Repository/Chrome/android.widget.EditText'), name.toString(), 0)

Mobile.delay(3)
Mobile.tap(findTestObject('Object Repository/Chrome/android.widget.Button - GET OTP CHROME'), 0)

Mobile.delay(5)
def act = Mobile.getText(findTestObject('Object Repository/Chrome/OTP'), 0)
act = act.replaceAll("\\D+","")
Mobile.comment(act)

//act = Mobile.getText(findTestObject('Object Repository/Chrome/OTP'), 0)

Mobile.pressHome()

Mobile.tap(findTestObject('Object Repository/Chuck/android.widget.TextView - (Dev)Brilink Mobile'), 0)

//tap button kirim

Mobile.setText(findTestObject('Object Repository/Info Saldo/android.widget.EditText - Kode OTP'), act, 0)
Mobile.tap(findTestObject('Object Repository/Info Saldo/Button - KIRIM kode OTP'), 0)


Mobile.callTestCase(findTestCase('Test Cases/Chuck/Notifikasi Chuck'), null)

Mobile.pressHome()

Mobile.tap(findTestObject('Object Repository/Chuck/android.widget.TextView - (Dev)Brilink Mobile'), 0)