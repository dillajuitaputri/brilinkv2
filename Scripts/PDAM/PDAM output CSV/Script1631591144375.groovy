import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

////verify trx success
//Mobile.verifyElementExist(findTestObject('Object Repository/Tiket Pesawat/csv/TextView - Sukses'), 0)

CustomKeywords.'common.screenshot.takeScreenshot'()

//nmr referensi
String refnum = Mobile.getText(findTestObject('Object Repository/PDAM/android.widget.TextView - 100001299178'), 0)
Mobile.comment(refnum)

////tipeataujenispembayaran
//String tipe = Mobile.getText(findTestObject('Object Repository/PDAM/android.widget.TextView - AETRA Jakarta'), 0)
//Mobile.comment(tipe)

//tgl trx
Mobile.scrollToText('Admin Bank')
String time = Mobile.getText(findTestObject('Object Repository/PDAM/tanggal'), 0)
Mobile.comment(time)

//jumlah pembayaran
String amount = Mobile.getText(findTestObject('Object Repository/PDAM/jumlah'), 0)
String nominal = amount.replaceAll(",00", "")
Mobile.comment(nominal)

//admin
String admin = Mobile.getText(findTestObject('Object Repository/PDAM/admin'), 0)
String fee = admin.replaceAll(",00", "")
Mobile.comment(admin)

//total pembayaran
String total = Mobile.getText(findTestObject('Object Repository/PDAM/total'), 0)
String jumlah = total.replaceAll(",00", "")
Mobile.comment(total)

Scanner sc = new Scanner(new File("customer.csv"))
String strNumber
while(sc.hasNext()) {
	String filePerLine = sc.nextLine()
	String[] data = filePerLine.split(",")
	strNumber = data[0].trim();
}
int number = 1
FileWriter writer = new FileWriter("customer.csv",true);
if (strNumber != null) {
	writer.write("\n")
	number = Integer.parseInt(strNumber)
	number = number + 1
}
Mobile.comment(feature.toString())
writer.write(number + "," +feature.toString() +","+tipe+","+time+","+refnum+","+nominal+","+fee+","+jumlah);
sc.close()
writer.close();

//CustomKeywords.'common.other.fileWrite'(feature, time, refnum, amount, fee)

CustomKeywords.'common.screenshot.takeScreenshot'()

GlobalVariable.reportStatus = "success"

