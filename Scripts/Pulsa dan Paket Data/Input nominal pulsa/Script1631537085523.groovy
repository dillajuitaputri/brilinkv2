import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

def workBook = ExcelKeywords.getWorkbook("Datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Pulsa dan Paket Data")

String nominalPulsa = ExcelKeywords.getCellValueByAddress(sheet1, 'B'+GlobalVariable.rowNumber)

//set text
switch (nominalPulsa) {
			case '5000':
//				CustomKeywords.'screenshot.capture.Screenshot'()

				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 5.000'), 0)

//				GlobalVariable.typeGopay = 'Customer'

				break
			case '10000':
//				CustomKeywords.'screenshot.capture.Screenshot'()

				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 10.000'), 0)

//				GlobalVariable.typeGopay = 'Driver'

				break
			case '15000':
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 15.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '20000':
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 20.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '25000':
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 25.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '30000':
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 30.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '40000':
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 40.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '50000':
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 50.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'
				break
				default:
					break
			}
Mobile.scrollToText('')
 
switch (nominalPulsa) {
			case '75000':
				Mobile.scrollToText('100.000')
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 75.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '100000':
				Mobile.scrollToText('100.000')
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 100.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '200000':
			Mobile.scrollToText('100.000')
			Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 200.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '300000':
			Mobile.scrollToText('100.000')
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 300.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '500000':
			Mobile.scrollToText('1.000.000')
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 500.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '1000000':
			Mobile.scrollToText('1.000.000')
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Nominal Pulsa/TextView - 1.000.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			default:
				break
		}

		Mobile.delay(3)
		
