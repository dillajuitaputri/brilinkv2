import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/actionbar - data'), 0)

def workBook = ExcelKeywords.getWorkbook("Datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Pulsa dan Paket Data")

String nominalPaketData = ExcelKeywords.getCellValueByAddress(sheet1, 'C'+GlobalVariable.rowNumber)

//ss
CustomKeywords.'common.screenshot.takeScreenshot'()

//set text
switch (nominalPaketData) {
			case '20000':
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Paket Data/TextView - 20.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '25000':
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Paket Data/TextView - 25.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '50000':
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Paket Data/TextView - 50.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'
				break

			case '100000':
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Paket Data/TextView - 100.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '150000':
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Paket Data/TextView - 150.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '200000':
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Paket Data/TextView - 200.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'

				break
			case '300000':
				Mobile.tap(findTestObject('Object Repository/Pulsa dan Paket Data/Paket Data/TextView - 300.000'), 0)

//				GlobalVariable.typeGopay = 'Merchant'
				break
			default:
				break
		}

//ss
CustomKeywords.'common.screenshot.takeScreenshot'()