import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

import com.kms.katalon.core.configuration.RunConfiguration
def workBook = ExcelKeywords.getWorkbook("Datasets/" + GlobalVariable.filePath)
def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Common")
String username = ExcelKeywords.getCellValueByAddress(sheet1, 'A2')
String password = ExcelKeywords.getCellValueByAddress(sheet1, 'B2')
String pvj = ExcelKeywords.getCellValueByAddress(sheet1, 'G2')

//pertamakali aktivasi
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/widget.ImageButton'), 0)
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/widget.ImageButton'), 0)
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/widget.ImageButton'), 0)
if (Mobile.verifyElementExist(findTestObject('Object Repository/Login Aktivasi/Button - GRANT PERMISSIONS'), 3, FailureHandling.OPTIONAL) == true) {
	Mobile.tap(findTestObject('Object Repository/Login Aktivasi/Button - GRANT PERMISSIONS'), 0)
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/Button - ALLOW'), 0)
Mobile.delay(3)
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/Button - ALLOW'), 0)
Mobile.delay(3)
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/Button - ALLOW'), 0)
Mobile.delay(3)
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/widget.ImageButton'), 0)
}
else {
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/widget.ImageButton'), 0)
}
//atur pvj
Mobile.delay(3)
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/brilinkmobile'), 0)
Mobile.delay(3)
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/Button - 2. Atur web service'), 0)
if (Mobile.verifyElementExist(findTestObject('Object Repository/Login Aktivasi/setpvj'), 3, FailureHandling.OPTIONAL) == true) {
Mobile.setText(findTestObject('Object Repository/Login Aktivasi/setpvj'), pvj.toString(), 0)
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/android.widget.Button - SIMPAN'), 0)
}
else {
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/android.widget.Button - SIMPAN'), 0)
}
//ss
//CustomKeywords.'common.screenshot.takeScreenshot'()

//kondisi 10 menit
//String data = 'johanf1819'
//CustomKeywords.'common.db.executeUpdate'(('UPDATE tbl_user  SET login_status = "0" WHERE username =  "' + data.toString()) + '"')

//input username dan password
Mobile.setText(findTestObject('Object Repository/General/Username'), username, 0)
Mobile.setText(findTestObject('Object Repository/General/Password'), password, 0)
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/android.widget.Button - AKTIVASI'), 0)

//ss
//CustomKeywords.'common.screenshot.takeScreenshot'()

//untuk dapat imei dan imsi
if (Mobile.verifyElementExist(findTestObject('Object Repository/Login Aktivasi/info aktivasi no hp'), 3, FailureHandling.OPTIONAL) == true) {
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/Button - YA'), 0)
}
else {
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/android.widget.Button - SMS (1)'), 0)}
Mobile.delay(5)
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/android.widget.Button - OK sms'), 0)
Mobile.delay(3)
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/back'), 0)
Mobile.delay(3)

//masuk apk
Mobile.startExistingApplication('id.co.bri.brilinkmobile')

//masukDB
def act = Mobile.getText(findTestObject('Object Repository/Login Aktivasi/imei imsi'), 0)
act = act.replaceAll("\\D+","")
Mobile.comment(act)

//pushtoDB
String imei = act.substring(0,16)
String imsi = act.substring(16,32)
String actStatus = '1'
Mobile.comment(imei)
Mobile.comment(imsi)
String name = 'johanf1819'
Mobile.callTestCase(findTestCase('Test Cases/Connect DB/connectDB'), [:], FailureHandling.STOP_ON_FAILURE)
CustomKeywords.'common.db.executeUpdate'(('UPDATE tbl_bm_activation SET imei = "' + imei.toString() + '" , imsi = "' + imsi.toString() + '" , activation_status = "' + actStatus.toString() + '" WHERE username = "' + name.toString()) + '"')

//OKactivation
Mobile.tap(findTestObject('Object Repository/Login Aktivasi/OK imei'), 0)