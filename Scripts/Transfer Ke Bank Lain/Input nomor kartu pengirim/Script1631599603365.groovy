import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

def workBook = ExcelKeywords.getWorkbook("Datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Transfer Bank Lain")

String noKartu = ExcelKeywords.getCellValueByAddress(sheet1, 'A'+GlobalVariable.rowNumber)

String tanggalKadaluarsa = ExcelKeywords.getCellValueByAddress(sheet1, 'C'+GlobalVariable.rowNumber)

String rekTujuan = ExcelKeywords.getCellValueByAddress(sheet1, 'D'+GlobalVariable.rowNumber)

String jumlahTrf = ExcelKeywords.getCellValueByAddress(sheet1, 'E'+GlobalVariable.rowNumber)

String noRef = ExcelKeywords.getCellValueByAddress(sheet1, 'F'+GlobalVariable.rowNumber)

//ss
CustomKeywords.'common.screenshot.takeScreenshot'()

//set text
Mobile.setText(findTestObject('Object Repository/Transfer BRI/EditText - Nomor Kartu ATM Pengirim'), noKartu.toString(), 0)

Mobile.setText(findTestObject('Object Repository/Transfer BRI/EditText - Tanggal Kadaluarsa Kartu'), tanggalKadaluarsa.toString(), 0)

Mobile.callTestCase(findTestCase('Test Cases/Transfer Bank Lain/Cari nama bank'), null)

Mobile.setText(findTestObject('Object Repository/Transfer BRI/EditText - Rekening Tujuan'), rekTujuan.toString(), 0)

Mobile.setText(findTestObject('Object Repository/Transfer BRI/EditText - Jumlah Transfer'), jumlahTrf.toString(), 0)

Mobile.setText(findTestObject('Object Repository/Transfer Bank Lain/EditText - Nomor Referensi'), noRef.toString(), 0)

//ss
CustomKeywords.'common.screenshot.takeScreenshot'()

//tap button kirim
Mobile.tap(findTestObject('Object Repository/Transfer BRI/Button - KIRIM'), 0)
