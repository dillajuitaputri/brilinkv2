import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import java.util.Scanner;
import java.io.File;
import java.io.FileWriter
import java.io.IOException;
import io.appium.java_client.android.AndroidDriver
//text transaksi sukses
//Mobile.verifyElementExist(findTestObject('Object Repository/Common/Trx Success/android.widget.TextView - Sukses'),0)
//Mobile.verifyElementExist(findTestObject('Object Repository/Common/Trx Success/Admin Bank'),0)

CustomKeywords.'common.screenshot.takeScreenshot'()

//tgl trx
String time = Mobile.getText(findTestObject('Object Repository/Briva/CSV/tanggal'), 0)
//Mobile.comment(time)

//nmr referensi
String refnum = Mobile.getText(findTestObject('Object Repository/Briva/CSV/android.widget.TextView - 100001296073'),0)
Mobile.comment(refnum)

//jumlah pembayaran
String amount = Mobile.getText(findTestObject('Object Repository/Briva/CSV/android.widget.TextView - Rp 150.000'),0)
Mobile.comment(amount)

//amount = amount.replaceAll(",00", "")

//admin
def fee = Mobile.getText(findTestObject('Object Repository/Briva/CSV/android.widget.TextView - Rp 2.000'),0)

//fee = fee.replaceAll(",00", "")

Mobile.comment(fee)

//total
def total = Mobile.getText(findTestObject('Object Repository/Briva/CSV/android.widget.TextView - Rp 152.000'),0)
Mobile.comment(total)

Scanner sc = new Scanner(new File("customer.csv"))
String strNumber
while(sc.hasNext()) {
	String filePerLine = sc.nextLine()
	String[] data = filePerLine.split(",")
	strNumber = data[0].trim();
}
int number = 1
FileWriter writer = new FileWriter("customer.csv",true);
if (strNumber != null) {
	writer.write("\n")
	number = Integer.parseInt(strNumber)
	number = number + 1
}
writer.write(number + "," +feature.toString() +","+time+","+refnum+","+amount+","+fee+ "," + total);
sc.close()
writer.close();

//CustomKeywords.'common.other.fileWrite'(feature, time, refnum, amount, fee)

CustomKeywords.'common.screenshot.takeScreenshot'()

GlobalVariable.reportStatus = "success"

