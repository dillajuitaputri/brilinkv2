import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//text transaksi sukses
//Mobile.verifyElementExist(findTestObject('Object Repository/Common/Trx Success/android.widget.TextView - Sukses'),0)
//Mobile.verifyElementExist(findTestObject('Object Repository/Common/Trx Success/Admin Bank'),0)

//CustomKeywords.'common.screenshot.takeScreenshot'()

//tanggal
String date = Mobile.getText(findTestObject('Object Repository/BRI Finance/csv/tgl'),0)
Mobile.comment(date)

//nmr referensi
String refnum = Mobile.getText(findTestObject('Object Repository/BRI Finance/csv/ca-noref-2'),0)
Mobile.comment(refnum)

//jumlah pembayaran
String amount = Mobile.getText(findTestObject('Object Repository/BRI Finance/csv/nominal'),0)
Mobile.comment(amount)

//admin
def fee = Mobile.getText(findTestObject('Object Repository/BRI Finance/csv/admin'),0)

//total
def total = Mobile.getText(findTestObject('Object Repository/BRI Finance/csv/total'),0)
Mobile.comment(total)

Scanner sc = new Scanner(new File("customer.csv"))
String strNumber
while(sc.hasNext()) {
	String filePerLine = sc.nextLine()
	String[] data = filePerLine.split(",")
	strNumber = data[0].trim();
}
int number = 1
FileWriter writer = new FileWriter("customer.csv",true);
if (strNumber != null) {
	writer.write("\n")
	number = Integer.parseInt(strNumber)
	number = number + 1
}
writer.write(number + "," +feature.toString() +","+date+","+refnum+","+amount+","+fee+total);
sc.close()
writer.close();


CustomKeywords.'common.screenshot.takeScreenshot'()

GlobalVariable.reportStatus = "success"

