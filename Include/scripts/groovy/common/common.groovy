package common
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class common {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("Case (\\d+)")
	def case_row_number(int rowNumber) {
		Mobile.comment("Execute file " + GlobalVariable.filePath)
		rowNumber = rowNumber + 1
		GlobalVariable.rowNumber = rowNumber
		Mobile.comment("GlobalVariable.rowNumber = " + GlobalVariable.rowNumber)
	}

	@Given("I run the application")
	def i_run_the_application(){
		Mobile.callTestCase(findTestCase('Test Cases/General/Run Application'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Given("I run the application BRILink")
	def i_run_the_application_brilink(){
		Mobile.callTestCase(findTestCase('Test Cases/General/Run BRILink'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I try login with existing account")
	def i_try_login_with_existing_account() {
		Mobile.callTestCase(findTestCase('Test Cases/Login/LoginCondition'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("I input password")
	def i_input_password() {
		Mobile.callTestCase(findTestCase('Test Cases/General/Input Password'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("Input password for New Feature")
	def i_input_password_new() {
		Mobile.callTestCase(findTestCase('Test Cases/General/Input Password New'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("I input wrong password")
	def i_input_wrong_password() {
		Mobile.callTestCase(findTestCase('Test Cases/General/Input Wrong Password'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Session over")
	def session_over() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Session Habis'), null)
	}

	@And("Session over before OTP")
	def session_over_before_OTP() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Session Habis OTP'), null)
	}

	@And("I tried to enter the wrong password three times")
	def i_input_wrong_password_three_times() {
		Mobile.callTestCase(findTestCase('Test Cases/General/Input Wrong Password Three Times'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Transaction success (.*)")
	def transaction_success(String feature) {
		Mobile.callTestCase(findTestCase('Test Cases/General/Transaction Success'), [('feature'):feature], FailureHandling.STOP_ON_FAILURE)
	}

	@And("I input password kartu kredit")
	def password_kartu_kredit() {
		Mobile.callTestCase(findTestCase('Test Cases/Kartu Kredit/Payment Kartu Kredit'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I click button KIRIM")
	def i_click_button_KIRIM() {
		Mobile.callTestCase(findTestCase('Test Cases/General/Click Button Kirim'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}