package baznas
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class baznas {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("I tap menu BAZNAS")
	def i_tap_menu_baznas() {
		Mobile.callTestCase(findTestCase('Test Cases/Dashboard/Click Menu BAZNAS'), [:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I choose Pembayaran")
	def i_choose_pembayaran() {
		Mobile.callTestCase(findTestCase('Test Cases/BAZNAS/Pembayaran/Choose Pembayaran Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I choose ID Pembayaran")
	def i_choose_id_pembayaran() {
		Mobile.callTestCase(findTestCase('Test Cases/BAZNAS/Pembayaran/Choose ID Pembayaran'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I choose Tipe Pembayaran")
	def i_choose_tipe_pembayaran() {
		Mobile.callTestCase(findTestCase('Test Cases/BAZNAS/Pembayaran/Choose Tipe Pembayaran'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input Nominal")
	def i_input_nominal() {
		Mobile.callTestCase(findTestCase('Test Cases/BAZNAS/Pembayaran/Input Nominal'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I choose Registrasi")
	def i_choose_registrasi() {
		Mobile.callTestCase(findTestCase('Test Cases/BAZNAS/Register/Choose Registrasi Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I choose ID Registrasi")
	def i_choose_id_registrasi() {
		Mobile.callTestCase(findTestCase('Test Cases/BAZNAS/Register/Choose ID Registrasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input NIK")
	def i_input_data_nik() {
		Mobile.callTestCase(findTestCase('Test Cases/BAZNAS/Register/Input NIK'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input Nama")
	def i_input_data_nama() {
		Mobile.callTestCase(findTestCase('Test Cases/BAZNAS/Register/Input Nama'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input Alamat")
	def i_input_data_alamat() {
		Mobile.callTestCase(findTestCase('Test Cases/BAZNAS/Register/Input Alamat'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Transcation BAZNAS Pembayaran success (.*)")
	def trx_success_baznas_pembayaran(String feature) {
		Mobile.callTestCase(findTestCase('Test Cases/BAZNAS/Pembayaran/BAZNAS Pembayaran Output CSV'), [('feature') : feature], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("Get data BAZNAS Pembayaran transaction success")
	def get_data_trx_baznas_pembayaran() {
		Mobile.callTestCase(findTestCase('Test Cases/BAZNAS/Pembayaran/Get Data Transaction Success'), [:],FailureHandling.STOP_ON_FAILURE)
	}

}