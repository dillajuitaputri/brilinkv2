package cicilan
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class cicilan {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("I tap menu Cicilan")
	def i_tap_menu_cicilan() {
		Mobile.callTestCase(findTestCase('Test Cases/Dashboard/Click Menu Cicilan'), [:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I choose Jenis Cicilan")
	def i_choose_jenis_cicilan() {
		Mobile.callTestCase(findTestCase('Test Cases/Cicilan/Choose Jenis Cicilan'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input Kode Pembayaran dan simpan ke daftar simpan")
	def daftar_simpan() {
		Mobile.callTestCase(findTestCase('Test Cases/Cicilan/Form/Input Dan Simpan Kode Pembayaran'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I search from daftar simpan")
	def search_daftar_simpan() {
		Mobile.callTestCase(findTestCase('Test Cases/Cicilan/Form/Cari daftar simpan'), null)
	}

	@When("I input Kode Pembayaran")
	def i_input_kode_pembayaran() {
		Mobile.callTestCase(findTestCase('Test Cases/Cicilan/Input Kode Pembayaran'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Transaction Cicilan success (.*)")
	def trx_success_cicilan(String feature) {
		Mobile.callTestCase(findTestCase('Test Cases/Cicilan/Output CSV'), [('feature'):feature], FailureHandling.STOP_ON_FAILURE)
	}
}