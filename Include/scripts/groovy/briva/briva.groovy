package briva
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class briva {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("I click menu Briva")
	def I_click_menu_briva() {
		Mobile.callTestCase(findTestCase('Test Cases/Dashboard/Click Menu Briva'), null)
	}

	@When("I input Nomor Pembayaran Briva")
	def I_input_nomor_pembayaran_briva() {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Input kode pembayaran'), null)
	}

	@When("I input Nominal Pembayaran Briva")
	def I_input_nominal_pembayaran_briva() {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Input nominal pembayaran'), null)
	}

	@When("Connection off")
	def Connection_off() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Connection Off'), null)
	}

	@Then("Transaction Briva success (.*)")
	def briva_success(String feature) {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Briva Output CSV'), [('feature') : feature], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Need help or go to catatan aktivitas briva")
	def need_help() {
		Mobile.callTestCase(findTestCase('Test Cases/General/CHS/Pilih Bantuan atau Catatan Aktivitas Briva'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}