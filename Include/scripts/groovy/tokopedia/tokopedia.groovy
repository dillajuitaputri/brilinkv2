package tokopedia
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class tokopedia {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("I tap menu Tokopedia")
	def i_tap_menu_tokopedia() {
		Mobile.callTestCase(findTestCase('Test Cases/Dashboard/Click Menu Tokopedia'), [:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input Kode Pembayaran Tokopedia")
	def i_input_kode_pembayaran_tokped() {
		Mobile.callTestCase(findTestCase('Test Cases/Tokopedia/Input Kode Pembayaran Tokopedia'), [:],FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("Transaction Tokopedia success (.*)")
	def transaction_success(String feature) {
		Mobile.callTestCase(findTestCase('Test Cases/Tokopedia/Tokopedia Output CSV'), [('feature'):feature], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Get data Tokopedia transaction success")
	def get_data_trx_tokopedia() {
		Mobile.callTestCase(findTestCase('Test Cases/Tokopedia/Get Data Transaction Success'), [:],FailureHandling.STOP_ON_FAILURE)
	}
}