package teleponPascabayar
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class teleponPascabayar {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("I click menu Telepon Pascabayar")
	def I_click_menu_telepon_pascabayar() {
		Mobile.callTestCase(findTestCase('Test Cases/Dashboard/Click Menu Telepon Pascabayar'), null)
	}

	@When("I input Jenis Transaksi")
	def I_input_jenis_transaksi() {
		Mobile.callTestCase(findTestCase('Test Cases/Telepon Pascabayar/Input Jenis Transaksi Telepon Pascabayar'), null)
	}
	
	@When("I input Nomor Telepon")
	def I_input_nomor_telepon() {
		Mobile.callTestCase(findTestCase('Test Cases/Telepon Pascabayar/Input Nomor Telepon Pascabayar'), null)
	}
	
	@Then("Transaction Telepon Pascabayar success (.*)")
	def trx_success_telepon_pascabayar(String feature) {
		Mobile.callTestCase(findTestCase('Test Cases/Telepon Pascabayar/Telepon Pascabayar Output CSV'), [('feature'):feature], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Get data Telepon Pascabayar transaction success")
	def get_data_trx_telepon_pascabayar() {
		Mobile.callTestCase(findTestCase('Test Cases/Telepon Pascabayar/Get Data Transaction Success'), [:],FailureHandling.STOP_ON_FAILURE)
	}
}