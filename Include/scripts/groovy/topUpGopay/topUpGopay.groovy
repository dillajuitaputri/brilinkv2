package topUpGopay
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class topUpGopay {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("I click menu Top Up Gopay")
	def I_click_menu_top_up_gopay() {
		Mobile.callTestCase(findTestCase('Test Cases/Dashboard/Click Menu Top Up Gopay'), null)
	}

	@When("I choose jenis transaksi Gopay")
	def I_choose_jenis_transaksi_gopay() {
		Mobile.callTestCase(findTestCase('Test Cases/Top Up Gopay/Input Jenis Transaksi Top Up Gopay'), null)
	}

	@When("I input Nomor dan Nominal Top Up Gopay")
	def I_input_nomor_dan_nominal_top_up_gopay() {
		Mobile.callTestCase(findTestCase('Test Cases/Top Up Gopay/Input Nomor dan Nominal Top Up Gopay'), null)
	}
	
	@Then("Transaction Gopay success (.*)")
	def transaction_gopay_success(String feature) {
		Mobile.callTestCase(findTestCase('Test Cases/Top Up Gopay/Top Up Gopay Output CSV'), [('feature'):feature], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Get data Gopay transaction success")
	def get_data_trx_gopay() {
		Mobile.callTestCase(findTestCase('Test Cases/Top Up Gopay/Get Data Transaction Success'), [:],FailureHandling.STOP_ON_FAILURE)
	}
}