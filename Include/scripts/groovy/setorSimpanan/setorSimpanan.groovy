package setorSimpanan
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class setorSimpanan {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
		@When("I tap menu Setor Simpanan")
	def i_tap_menu_setor_simpanan() {
		Mobile.callTestCase(findTestCase('Test Cases/Dashboard/Click Menu Setor Simpanan'), [:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input Nomor Rekening for Setor Simpanan Payment")
	def i_input_nomor_rekening_for_setor_simpan() {
		Mobile.callTestCase(findTestCase('Test Cases/Setor Simpanan/Input Nomor Rekening Setor Simpanan'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input Jumlah Setoran")
	def i_input_jumlah_setoran() {
		Mobile.callTestCase(findTestCase('Test Cases/Setor Simpanan/Input Jumlah Setoran Simpanan'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("Transaction Setor Simpan success (.*)")
	def trx_success_setor_simpanan(String feature) {
		Mobile.callTestCase(findTestCase('Test Cases/Setor Simpanan/Setor Simpanan Output CSV'), [('feature'):feature], FailureHandling.STOP_ON_FAILURE)
	}
	
	
	@When("Get data Setor Simpanan transaction success")
	def get_data_trx_setor_simpanan() {
		Mobile.callTestCase(findTestCase('Test Cases/Setor Simpanan/Get Data Transaction Success'), [:],FailureHandling.STOP_ON_FAILURE)
	}
}