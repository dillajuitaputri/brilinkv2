package pln
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class pln {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("I tap menu PLN")
	def i_tap_menu_pln() {
		Mobile.callTestCase(findTestCase('Test Cases/Dashboard/Click Menu PLN'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I choose Jenis Transaksi PLN")
	def i_choose_jenis_transaksi_pln() {
		Mobile.callTestCase(findTestCase('Test Cases/PLN/Choose Jenis Transaksi PLN'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input IDPEL atau Nomor Meteran")
	def i_input_ipdel_nomor_meteran() {
		Mobile.callTestCase(findTestCase('Test Cases/PLN/Input IPDEL atau Nomor Meteran'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I input IPDEL")
	def i_input_ipdel() {
		Mobile.callTestCase(findTestCase('Test Cases/PLN/I Input IDPEL'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I input Nomor Referensi")
	def i_input_nomor_referensi() {
		Mobile.callTestCase(findTestCase('Test Cases/PLN/I Input Nomor Referensi'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I input Nomor Meteran")
	def i_input_meteran() {
		Mobile.callTestCase(findTestCase('Test Cases/PLN/I Input Nomor Meteran'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I input Nomor Registrasi")
	def i_input_noreg() {
		Mobile.callTestCase(findTestCase('Test Cases/PLN/I Input Nomor Registrasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I choose Nominal")
	def i_choose_nominal() {
		Mobile.callTestCase(findTestCase('Test Cases/PLN/I Choose Nominal'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("Transaction PLN success (.*)")
	def trx_success_pln_pascabayar(String feature) {
		Mobile.callTestCase(findTestCase('Test Cases/PLN/Output CSV PLN'), [('feature'):feature], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Get data PLN transaction success")
	def get_data_trx_pln() {
		Mobile.callTestCase(findTestCase('Test Cases/PLN/Get Data Transaction Success'), [:],FailureHandling.STOP_ON_FAILURE)
	}
}