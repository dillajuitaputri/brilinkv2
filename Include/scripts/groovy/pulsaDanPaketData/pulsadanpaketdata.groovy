package pulsaDanPaketData
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class pulsadanpaketdata {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("I click menu Pulsa")
	def I_click_menu_pulsa() {
		Mobile.callTestCase(findTestCase('Test Cases/Dashboard/Click Menu Pulsa dan Paket Data'), null)
	}

	@When("I click menu Paket Data")
	def I_click_menu_paket_data() {
		Mobile.callTestCase(findTestCase('Test Cases/Dashboard/Click Menu Pulsa dan Paket Data'), null)
	}

	@When("I input Nomor Pulsa")
	def I_input_nomor_pulsa() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa dan Paket Data/Input nomor pulsa dan paket data'), null)
	}

	@When("I input Nomor Paket Data")
	def I_input_nomor_paket_data() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa dan Paket Data/Input nomor pulsa dan paket data'), null)
	}

	@When ("I search the number")
	def I_search_the_number() {
		Mobile. callTestCase(findTestCase('Test Cases/Pulsa dan Paket Data/Buku Telepon'), null)
	}

	@When("I choose nominal Pulsa")
	def I_choose_nominal_pulsa() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa dan Paket Data/Input nominal pulsa'), null)
	}

	@When("I choose Paket Data")
	def I_choose_paket_data() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa dan Paket Data/Input nominal paket data'), null)
	}
	
	@Then("Need help or go to catatan aktivitas paket data")
	def aktivitas_paketdata() {
		Mobile.callTestCase(findTestCase('Test Cases/General/CHS/Pilih Bantuan atau Catatan Altivitas Paket Data'), null)
	}

	@Then("Transaction Paket Data dan Pulsa success(.*)")
	def trx_success_paket_data_pulsa(String feature) {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa dan Paket Data/csv Paket Data dan Pulsa'), [('feature'):feature], FailureHandling.STOP_ON_FAILURE)
	}
}