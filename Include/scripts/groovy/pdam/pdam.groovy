package pdam
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class pdam {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("I tap menu PDAM")
	def i_tap_menu_pdam() {
		Mobile.callTestCase(findTestCase('Test Cases/Dashboard/Click Menu PDAM'), [:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I choose Daerah PDAM")
	def i_choose_daerah_pdam() {
		Mobile.callTestCase(findTestCase('Test Cases/PDAM/Input Daerah PDAM'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I search Daerah PDAM")
	def i_seach_daerah() {
		Mobile.callTestCase(findTestCase('Test Cases/PDAM/Cari Daerah'), [:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input ID Pelanggan")
	def i_input_id_pelanggan() {
		Mobile.callTestCase(findTestCase('Test Cases/PDAM/Input ID Pelanggan'), [:],FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Transaction PDAM success (.*)")
	def transaction_pdam(String feature) {
		Mobile.callTestCase(findTestCase('Test Cases/PDAM/PDAM output CSV'), [('feature'):feature], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("Need help or go to catatan aktivitas PDAM")
	def aktivitas_PDAM() {
		Mobile.callTestCase(findTestCase('Test Cases/General/CHS/Pilih Bantuan atau Catatan Aktivitas PDAM'), null)
	}
}