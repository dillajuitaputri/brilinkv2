package tiketKAI
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class tiketKAI {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("I click menu Tiket Kereta Api")
	def I_click_menu_tiket_kereta_api() {
		Mobile.callTestCase(findTestCase('Test Cases/Dashboard/Click Menu Tiket KAI'), null)
	}

	@Then("I input Nomor Tiket Kereta Api")
	def I_input_nomor_tiket_kereta_api() {
		Mobile.callTestCase(findTestCase('Test Cases/Tiket KAI/Input Nomor KAI'), null)
	}
	
	@Then("Transaction KAI success (.*)")
	def trx_success_kai(String feature) {
		Mobile.callTestCase(findTestCase('Test Cases/Tiket KAI/Tiket KAI Output CSV'), [('feature'):feature], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Get data Tiket KAI transaction success")
	def get_data_trx_tiket_KAI() {
		Mobile.callTestCase(findTestCase('Test Cases/Tiket KAI/Get Data Transaction Success'), [:],FailureHandling.STOP_ON_FAILURE)
	}
}