#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

   @tag1-TransactionBPJSKSDataValid
  Scenario Outline: BPJS Kesehatan Payment
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu BPJS Kesehatan
    When I input Nomor Pembayaran BPJS Kesehatan
    And I input password
    #Then Action struk
    Examples: 
      | case |
      |    1 |
      
  #@tag2
  #Scenario Outline: BPJS Kesehatan Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BPJS Kesehatan
    #When I input Nomor Pembayaran BPJS Kesehatan
    #And Session over
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    2 |
      
      #@tag3
  #Scenario Outline: BPJS Kesehatan Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BPJS Kesehatan
    #When I input Nomor Pembayaran BPJS Kesehatan
    #And Session over
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    3 |
      
      #@tag4
  #Scenario Outline: BPJS Kesehatan Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BPJS Kesehatan
    #When I input Nomor Pembayaran BPJS Kesehatan
    #And Session over
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    4 |
      
      #@tag5
  #Scenario Outline: BPJS Kesehatan Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BPJS Kesehatan
    #When I input Nomor Pembayaran BPJS Kesehatan
    #And Session over
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    5 |
      
      #@tag6
  #Scenario Outline: BPJS Kesehatan Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BPJS Kesehatan
    #When I input Nomor Pembayaran BPJS Kesehatan
    #And Session over
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    6 |
      
      #@tag7
  #Scenario Outline: Session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BPJS Kesehatan
    #When I input Nomor Pembayaran BPJS Kesehatan
    #And Session over
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    7 |
      
      #@tag8
  #Scenario Outline: Koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BPJS Kesehatan
    #When I input Nomor Pembayaran BPJS Kesehatan
    #And Session over
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
   #@tag9-TC009-TrxSuccessBPJSKSSentToEmail
  #Scenario Outline: BPJS Kesehatan Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BPJS Kesehatan
    #When I input Nomor Pembayaran BPJS Kesehatan
    #And I input password
    #Then Action struk
    #Then Transaction Receipt Sent to Email
    #
    #Examples: 
      #| case |
      #|    1 |
      #
   #@tag10-TC010-TrxSuccessBPJSKSSentToSMS
  #Scenario Outline: BPJS Kesehatan Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BPJS Kesehatan
    #When I input Nomor Pembayaran BPJS Kesehatan
    #And I input password
    #Then Action struk
    #Then Transaction Receipt Sent to SMS	
    #
    #Examples: 
      #| case |
      #|    1 |
      #
   #@tag11-TC011-TrxSuccessBPJSKSConvertPDF
  #Scenario Outline: BPJS Kesehatan Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BPJS Kesehatan
    #When I input Nomor Pembayaran BPJS Kesehatan
    #And I input password
    #Then Action struk
    #Then Transaction Receipt Save to PDF
    #
    #Examples: 
      #| case |
      #|    1 |