#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag1-TC001-KodePembayaranValid
  Scenario Outline: User melakukan transaksi KAI dengan data yang valid
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I click menu Tiket Kereta Api
    When I input Nomor Tiket Kereta Api
    And I input password
    When Get data Tiket KAI transaction success
    Then Transaction KAI success <feature>

    Examples: 
      | case | feature |
      |    1 | KAI |
      

#belum ada validasi brilinknya      
  #@tag2-TC002-KodePembayaranInValid
  #Scenario Outline: User memasukan Kode Pembayaran dengan data yang tidak valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Kereta Api
    #When I input Nomor Tiket Kereta Api
    #
    #Examples: 
      #| case |
      #|    2 |
      
      
  #@tag4-TC004-UserTidakMemasukanKodePembayaran
  #Scenario Outline: User tidak memasukan Kode Pembayaran
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Kereta Api
    #And I click button KIRIM in feature KAI
    #
    #Examples: 
      #| case |
      #|    1 |
      
  
  #case menggunakan akun yang tidak ada saldo     
  #@tag5-TC005-SaldoTidakCukup
  #Scenario Outline: User melakukan transaksi Tokopedia dengan kondisi saldo tidak cukup
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Kereta Api
    #When I input Nomor Tiket Kereta Api
    #And I input password
#
    #Examples: 
      #| case |
      #|    1 |
      #
      
  #@tag6-TC006-SalahInputPassword
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Kereta Api
    #When I input Nomor Tiket Kereta Api
 #		And I input wrong password
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag7-TC006-SalahInputPasswordTigaKali
  #Scenario Outline: User salah memasukkan password transaksi sebanyak 3 kali
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Kereta Api
    #When I input Nomor Tiket Kereta Api
    #And I input wrong password
#		Then I tried to enter the wrong password three times
#		Then I tried to enter the wrong password three times
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag8-TC008-SessionHabis
  #Scenario Outline: User melakukan transaksi KAI lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Kereta Api
    #When I input Nomor Tiket Kereta Api
    #And Session over  
#
    #Examples: 
      #| case |
      #|    1 |
      #
  #@tag9-TC009-KoneksiTerputus
  #Scenario Outline: User melakukan transaksi KAI lalu koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Kereta Api
    #When I input Nomor Tiket Kereta Api
    #When Connection off
#
    #Examples: 
      #| case |
      #|    1 |
      
   #@tag10-TC010-TrxSuccessTiketKAISentToEmail
  #Scenario Outline: User melakukan transaksi KAI dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Kereta Api
    #When I input Nomor Tiket Kereta Api
    #And I input password
    #Then Transaction KAI success <feature>
    #Then Transaction Receipt Sent to Email
#
    #Examples: 
      #| case | feature |
      #|    1 | KAI |
      #
   #@tag11-TC011-TrxSuccessTiketKAISentToSMS
  #Scenario Outline: User melakukan transaksi KAI dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Kereta Api
    #When I input Nomor Tiket Kereta Api
    #And I input password
    #Then Transaction KAI success <feature>
    #Then Transaction Receipt Sent to SMS	
#
    #Examples: 
      #| case | feature |
      #|    1 | KAI |
      #
   #@tag12-TC012-TrxSuccessTiketKAIConvertPDF
  #Scenario Outline: User melakukan transaksi KAI dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Kereta Api
    #When I input Nomor Tiket Kereta Api
    #And I input password
    #Then Transaction KAI success <feature>
    #Then Transaction Receipt Save to PDF
#
    #Examples: 
      #| case | feature |
      #|    1 | KAI |