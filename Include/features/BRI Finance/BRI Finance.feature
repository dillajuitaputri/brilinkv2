#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

   @tag1-TC01-PembayaranBRIFinance
  Scenario Outline: BRI Finance Payment
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu BRI Finance
    When I input Nomor Kontrak
    And Input password for New Feature
    Then Transaction BRI Finance success <feature>

    Examples: 
      | case | feature |
      |    1 | BRI Finance |