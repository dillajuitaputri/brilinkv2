#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

		@tag1
 		Scenario Outline: Nomor kartu dan tanggal expire benar
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I click menu Transfer Bank Lain
    When I input nomor kartu ATM pengirim
    And I choose nomor handphone
     And I input password
    #Then Action struk
    Examples: 
      | case |
      |    6 |
      
    #@tag2
 #		Scenario Outline: Nomor kartu benar tetapi tanggal expire salah
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    2 |
      #
      #@tag3
 #		Scenario Outline: Nomor kartu salah tetapi tanggal expire benar
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    3 |
      #
      #@tag4
 #		Scenario Outline: Nomor kartu dan tanggal expire salah
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    4 |
      #
      #@tag5
 #		Scenario Outline: Nomor kartu kurang dari 16 digit
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    5 |
      #
      #@tag6belum
 #		Scenario Outline: Cari daftar bank dengan kode bank
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I choose the bank
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    6 |
      #
      #@tag7belum
 #		Scenario Outline: Cari daftar bank tujuan dengan nama bank
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I choose the bank
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    7 |
      #
      #@tag8pilih
 #		Scenario Outline: Pilih bank tujuan
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@tag9
 #		Scenario Outline: Nomor rekening salah
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    9 |
      #
      #@tag10
 #		Scenario Outline: Jumlah transfer melebihi jumlah limit harian
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    10|
      #
      #@tag11
 #		Scenario Outline: Jumlah transfer kurang dari jumlah min. transfer 
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    11|
      #
      #@tag12
 #		Scenario Outline: Masukkan nomor referensi
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    12|
      #
      #@tag13
 #		Scenario Outline: Tidak memasukkan nomor referense
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    13|
      #
      #@tag14
 #		Scenario Outline: Kode OTP benar
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    14|
      #
      #@tag15
 #		Scenario Outline: Kode OTP salah
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    15|
      #
      #@tag16
 #		Scenario Outline: Terlalu lama memasukkan kode OTP
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    16|
      #
      #@tag17
 #		Scenario Outline: Salah memasukkan password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    17|
      #
      #@tag18
 #		Scenario Outline: Salah memasukkan password 3x
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    18|
      #
      #@tag19
 #		Scenario Outline: Tidak mengisi tanggal expire
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    19|
      #
      #@tag20
 #		Scenario Outline: Session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
    #And Session over 
    #Then Action struk
    #Examples: 
      #| case |
      #|    20|
      #
      #@tag21
 #		Scenario Outline: Koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    21|
      
      #@tag22
 #		Scenario Outline: Nomor handphone tidak terdaftar
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Transfer Bank Lain
    #When I input nomor kartu ATM pengirim
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    22|