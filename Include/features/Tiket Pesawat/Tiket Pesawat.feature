#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

 
 @tag1-TC001-GarudaIndonesia
  Scenario Outline: User memasukan nomor Tiket Pesawat Garuda Indonesia dengan data yang valid
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I click menu Tiket Pesawat
    When I input kode pembayaran Tiket Pesawat dan pilih maskapai
    And I input password
    When Get data Tiket Pesawat transaction success
    Then Transaction Tiket Pesawat success <feature>

    Examples: 
      | case | feature |
      |    1 | Tiket Pesawat Garuda Indonesia |
      
  #@tag2-TC001-LionAir
  #Scenario Outline: User memasukan nomor Tiket Pesawat Lion Air dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Pesawat
    #When I input kode pembayaran Tiket Pesawat dan pilih maskapai
    #And I input password
    #Then Transaction Tiket Pesawat success <feature>
    #Then Action struk
#
    #Examples: 
      #| case | feature |
      #|    2 | Tiket Pesawat Lion Air |
      #
      #
  #@tag3-TC003-SriwijayaAir
  #Scenario Outline: User memasukan nomor Tiket Pesawat Sriwijaya Air dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Pesawat
    #When I input kode pembayaran Tiket Pesawat dan pilih maskapai
    #And I input password
    #Then Transaction Tiket Pesawat success <feature>
    #Then Action struk
#
    #Examples: 
      #| case | feature |
      #|    3 | Tiket Pesawat Sriwijaya Air |
      
  #@tag2-TC002-DataInvalid
  #Scenario Outline: User memasukan nomor Tiket Pesawat dengan data yang tidak valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Pesawat
    #When I input kode pembayaran Tiket Pesawat dan pilih maskapai
#
    #Examples: 
      #| case |
      #|    2 |
      #
  #@tag4-TC004-UserTidakInputKodePembayaran
  #Scenario Outline: User tidak memasukan Kode Pembayaran
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Pesawat
#		Then I click button KIRIM in feature Tiket Pesawat
#
    #Examples: 
      #| case |
      #|    1 |
      
  #login menggunakan akun saldo tidak cukup    
  #@tag5-TC005-SaldoTidakCukup
  #Scenario Outline: User memasukan nomor Tiket Pesawat dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Pesawat
    #When I input kode pembayaran Tiket Pesawat dan pilih maskapai
    #And I input password
    #Then Action struk
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag6-TC006-SalahInputPassword
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Pesawat
    #When I input kode pembayaran Tiket Pesawat dan pilih maskapai
    #And I input wrong password
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag7-TC007-SalahInputPasswordTigaKali
  #Scenario Outline: User salah memasukkan password transaksi sebanyak 3 kali
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Pesawat
    #When I input kode pembayaran Tiket Pesawat dan pilih maskapai
    #And I input wrong password
#		Then I tried to enter the wrong password three times
#		Then I tried to enter the wrong password three times
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag8-TC008-SessionHabis
  #Scenario Outline: User melakukan transaksi Tiket Pesawat lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Pesawat
    #When I input kode pembayaran Tiket Pesawat dan pilih maskapai
    #And Session over    
#
    #Examples: 
      #| case |
      #|    1 |
      #
  #@tag9-TC009-KoneksiTerputus
  #Scenario Outline: User melakukan transaksi Tiket Pesawat lalu koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Pesawat
    #When I input kode pembayaran Tiket Pesawat dan pilih maskapai
    #When Connection off
#
    #Examples: 
      #| case |
      #|    1 |
      
   #@tag10-TC010-TrxSuccessGarudaIndonesiaSentToEmail
  #Scenario Outline: User memasukan nomor Tiket Pesawat Garuda Indonesia dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Pesawat
    #When I input kode pembayaran Tiket Pesawat dan pilih maskapai
    #And I input password
    #Then Transaction Tiket Pesawat success <feature>
    #Then Transaction Receipt Sent to Email
#
    #Examples: 
      #| case | feature |
      #|    1 | Tiket Pesawat Garuda Indonesia |
      #
      #
   #@tag11-TC011-TrxSuccessGarudaIndonesiaSentToSMS
  #Scenario Outline: User memasukan nomor Tiket Pesawat Garuda Indonesia dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Pesawat
    #When I input kode pembayaran Tiket Pesawat dan pilih maskapai
    #And I input password
    #Then Transaction Tiket Pesawat success <feature>
    #Then Transaction Receipt Sent to SMS
#
    #Examples: 
      #| case | feature |
      #|    1 | Tiket Pesawat Garuda Indonesia |
      #
      #
   #@tag12-TC012-TrxSuccessGarudaIndonesiaConvertPDF
  #Scenario Outline: User memasukan nomor Tiket Pesawat Garuda Indonesia dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Tiket Pesawat
    #When I input kode pembayaran Tiket Pesawat dan pilih maskapai
    #And I input password
    #Then Transaction Tiket Pesawat success <feature>
    #Then Transaction Receipt Save to PDF
#
    #Examples: 
      #| case | feature |
      #|    1 | Tiket Pesawat Garuda Indonesia |
      