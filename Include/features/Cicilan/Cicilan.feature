#Author: dillajuitaputri@gmail.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Feature Cicilan
  I want to use this template for my feature file

  @tag1-FIF
  Scenario Outline: Cicilan Finance
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu Cicilan
    When I choose Jenis Cicilan
    When I input Kode Pembayaran
    And I input password
    Then Transaction Cicilan success <feature>
    Examples: 
      | case | feature |
      |    1 | Cicilan FIF |
      
  #@tag1-OTO
  #Scenario Outline: Cicilan Finance
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran
    #And I input password
    #Then Transaction Cicilan success <feature>
    #Examples: 
      #| case | feature |
      #|    13 | Cicilan OTO |
      
  #@tag1-BAF
  #Scenario Outline: Cicilan Finance
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran
    #And I input password
    #Then Transaction Cicilan success <feature>
    #Examples: 
      #| case | feature |
      #|    14| Cicilan BAF |
      
  #@tag1-FINANSIA
  #Scenario Outline: Cicilan Finance
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran
    #And I input password
    #Then Transaction Cicilan success <feature>
    #Examples: 
      #| case | feature |
      #|    15| Cicilan FINANSIA |
      
  #@tag1-VERENA
  #Scenario Outline: Cicilan Finance
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran
    #And I input password
    #Then Transaction Cicilan success <feature>
    #Examples: 
      #| case | feature |
      #|    16| Cicilan VERENA |
      #
  @tag1-WOM
  Scenario Outline: Cicilan Finance
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu Cicilan
    When I choose Jenis Cicilan
    When I input Kode Pembayaran
    And I input password
    Then Transaction Cicilan success <feature>
    Examples: 
      | case | feature |
      |    17| Cicilan WOM |
      
  #@tag2
  #Scenario Outline: Data tidak valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran
    #And I input password
    #Examples: 
      #| case |
      #|    2 |
      
      #@tag3
  #Scenario Outline: Kode pembayaran kurang dari 8 digit
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran
    #And I input password
    #Examples: 
      #| case |
      #|    3 |
      
      #@tag4
  #Scenario Outline: Kode pembayaran tidak terdaftar
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran
    #And I input password
    #Examples: 
      #| case |
      #|    4 |
      
      #@tag5
  #Scenario Outline: Tidak memasukkan kode pembayaran
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran
    #Examples: 
      #| case |
      #|    5 |
      
      #@tag6
  #Scenario Outline: Transaksi melebihi saldo
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran
    #And I input password
    #Examples: 
      #| case |
      #|    6 |
      
      #@tag7
  #Scenario Outline: Salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran
    #And I input password
    #Examples: 
      #| case |
      #|    7 |
      
      #@tag8
  #Scenario Outline: Salah input password 3x
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran
    #And I input password
    #Examples: 
      #| case |
      #|    8 |
      
      #@tag9
  #Scenario Outline: Simpan kode pembayaran
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran dan simpan ke daftar simpan
    #And I input password
    #Examples: 
      #| case |
      #|    9 |
      
      #@tag10
  #Scenario Outline: Cari daftar simpan
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I search from daftar simpan
    #When I input Kode Pembayaran
    #And I input password
    #Examples: 
      #| case |
      #|    10|
      
      #@tag11
  #Scenario Outline: Session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran
    #And I input password
    #Examples: 
      #| case |
      #|    11|
      
      #@tag12
  #Scenario Outline: Koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Cicilan
    #When I choose Jenis Cicilan
    #When I input Kode Pembayaran
    #And I input password
    #Examples: 
      #| case |
      #|    12|