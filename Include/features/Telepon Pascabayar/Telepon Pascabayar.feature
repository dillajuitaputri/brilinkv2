#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

 #	@tag1-TC001-DataNomorTeleponValid
  #Scenario Outline: User melakukan transaksi dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Telepon Pascabayar
    #When I input Jenis Transaksi
    #When I input Nomor Telepon
    #And I click button KIRIM
    #And I input password
    #When Get data Telepon Pascabayar transaction success
    #Then Transaction Telepon Pascabayar success <feature>
#
#
    #Examples: 
      #| case | feature |
      #|    1 | Telepon Pascabayar |
      #
      	@tag1-TC001-DataNomorTeleponValid
  Scenario Outline: User melakukan transaksi dengan data yang valid
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I click menu Telepon Pascabayar
    When I input Jenis Transaksi
    When I input Nomor Telepon
    And I click button KIRIM
    And I input password
    When Get data Telepon Pascabayar transaction success
    Then Transaction Telepon Pascabayar success <feature>


    Examples: 
      | case | feature |
      |    1 | Telepon Pascabayar |
      
  #@tag2-TC002-DataNomorTeleponInValid
  #Scenario Outline: User melakukan transaksi dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Telepon Pascabayar
    #When I input Jenis Transaksi
    #When I input Nomor Telepon
    #And I click button KIRIM
#
    #Examples: 
      #| case |
      #|    2 |
      
  #@tag3-TC003-DataNomorTeleponValid
  #Scenario Outline: User tidak memasukan Nomor Telepon
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Telepon Pascabayar
    #When I input Jenis Transaksi
   #	And I click button KIRIM
#
    #Examples: 
      #| case |
      #|    1 |
      
  #login menggunakan saldo yang tidak cukup    
  #@tag4-TC004-DataNomorTeleponValid
  #Scenario Outline: User melakukan transaksi melebihi saldo
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Telepon Pascabayar
    #When I input Jenis Transaksi
    #When I input Nomor Telepon
    #And I click button KIRIM
    #And I input password
    #Then Action struk
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag5-TC005-SalahInputPassword
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Telepon Pascabayar
    #When I input Jenis Transaksi
    #When I input Nomor Telepon
    #And I click button KIRIM
    #And I input wrong password
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag6-TC006-SalahInputPassword
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Telepon Pascabayar
    #When I input Jenis Transaksi
    #When I input Nomor Telepon
    #And I click button KIRIM
    #And I input wrong password
   #	Then I tried to enter the wrong password three times
#		Then I tried to enter the wrong password three times
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag7-TC007-SessionHabis
  #Scenario Outline: User melakukan transaksi Telepon Pascabayar lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Telepon Pascabayar
    #When I input Jenis Transaksi
    #When I input Nomor Telepon
    #And I click button KIRIM
#		And Session over 
#
    #Examples: 
      #| case |
      #|    1 |
      #
  #@tag8-TC008-KoneksiTerputus
  #Scenario Outline: User melakukan transaksi Telepon Pascabayar lalu koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Telepon Pascabayar
    #When I input Jenis Transaksi
    #When I input Nomor Telepon
    #And I click button KIRIM
#		When Connection off
#
    #Examples: 
      #| case |
      #|    1 |
      
   #@tag9-TC009-TrxSuccessTeleponPascabayarSentToEmail
  #Scenario Outline: User melakukan transaksi dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Telepon Pascabayar
    #When I input Jenis Transaksi
    #When I input Nomor Telepon
    #And I click button KIRIM
    #And I input password
    #Then Transaction Telepon Pascabayar success <feature>
#		Then Transaction Receipt Sent to Email
#
    #Examples: 
      #| case | feature |
      #|    1 | Telepon Pascabayar |
      #
   #@tag10-TC010-TrxSuccessTeleponPascabayarSentToSMS
  #Scenario Outline: User melakukan transaksi dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Telepon Pascabayar
    #When I input Jenis Transaksi
    #When I input Nomor Telepon
    #And I click button KIRIM
    #And I input password
    #Then Transaction Telepon Pascabayar success <feature>
#		Then Transaction Receipt Sent to SMS	
#
    #Examples: 
      #| case | feature |
      #|    1 | Telepon Pascabayar |
      #
   #@tag11-TC011-TrxSuccessTeleponPascabayarConvertPDF
  #Scenario Outline: User melakukan transaksi dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Telepon Pascabayar
    #When I input Jenis Transaksi
    #When I input Nomor Telepon
    #And I click button KIRIM
    #And I input password
    #Then Transaction Telepon Pascabayar success <feature>
#		Then Transaction Receipt Save to PDF
#
    #Examples: 
      #| case | feature |
      #|    1 | Telepon Pascabayar |
      