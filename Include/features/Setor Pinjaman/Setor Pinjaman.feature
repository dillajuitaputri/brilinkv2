#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Feature Setor Pinjaman
  I want to use this template for my feature file

@tag1-TC001-KodePembayaranValid
  Scenario Outline: Setor Pinjaman Payment
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu Setor Pinjaman
    When I input Nomor Rekening Pinjaman
    When I input Nominal Setor Pinjaman
    And I input password
    When Get data Setor Pinjaman transaction success
    Then Transaction Setor Pinjaman success <feature>
  

    Examples: 
      | case | feature |
      |    1 | Setor Pinjaman |
      
  #@tag2-TC002-KodePembayaranInValidTidakBolehNol
  #Scenario Outline: User memasukan nomor Setor Pinjaman dengan data yang tidak valid (00000000)
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Pinjaman
    #When I input Nomor Rekening Pinjaman
#
    #Examples: 
      #| case |
      #|    2 |
      
  #@tag3-TC003-KodePembayaranInValidTidakBolehNol
  #Scenario Outline: User memasukan nomor Setor Pinjaman dengan data yang tidak valid (00000000)
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Pinjaman
    #When I input Nomor Rekening Pinjaman
#
    #Examples: 
      #| case |
      #|    3 |
      
   #@tag4-TC004-TidakInputKodePembayaran
  #Scenario Outline: User tidak memasukan Kode Pembayaran
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Pinjaman
    #When I click button KIRIM
    #
    #Examples: 
      #| case |
      #|    1 |
      
#menggunakan akun yang saldo tidak cukup/input nominal melebihi saldo  
  #@tag5-TC005-SaldoTidakCukup
  #Scenario Outline: User memasukan nominal melebihi saldo
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Pinjaman
    #When I input Nomor Rekening Pinjaman
    #When I input Nominal Setor Pinjaman
    #And I input password
#
    #Examples: 
      #| case |
      #|    4 |

#belum ada validasi brilinknya
  #@tag6-TC006-NominalKurangDariMinimal
  #Scenario Outline: User memasukan nominal kurang dari minimal 
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Pinjaman
    #When I input Nomor Rekening Pinjaman
    #When I input Nominal Setor Pinjaman
    #And I input password
#
    #Examples: 
      #| case |
      #|    5 |
      
  #@tag7-TC007-SalahInputPassword
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Pinjaman
    #When I input Nomor Rekening Pinjaman
    #When I input Nominal Setor Pinjaman
    #And I input wrong password
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag8-TC008-SalahInputPasswordTigaKali
  #Scenario Outline: User salah memasukkan password transaksi sebanyak 3 kali
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Pinjaman
    #When I input Nomor Rekening Pinjaman
    #When I input Nominal Setor Pinjaman
    #And I input wrong password
#		Then I tried to enter the wrong password three times
#		Then I tried to enter the wrong password three times
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag9-TC009-SessionHabis
  #Scenario Outline: User melakukan transaksi Setor Pinjaman lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Pinjaman
    #When I input Nomor Rekening Pinjaman
    #When I input Nominal Setor Pinjaman
#		And Session over
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag10-TC010-KoneksiTerputus
  #Scenario Outline: User melakukan transaksi Setor Pinjaman lalu koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Pinjaman
    #When I input Nomor Rekening Pinjaman
    #When I input Nominal Setor Pinjaman
#		When Connection off
#
    #Examples: 
      #| case |
      #|    1 |
      
   #@tag11-TC011-TrxSuccessSetorPinjamanSentToEmail
  #Scenario Outline: Setor Pinjaman Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Pinjaman
    #When I input Nomor Rekening Pinjaman
    #When I input Nominal Setor Pinjaman
    #And I input password
    #Then Transaction Setor Pinjaman success <feature>
    #Then Transaction Receipt Sent to Email
#
    #Examples: 
      #| case | feature |
      #|    1 | Setor Pinjaman |
      #
   #@tag12-TC012-TrxSuccessSetorPinjamanSentToSMS
  #Scenario Outline: Setor Pinjaman Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Pinjaman
    #When I input Nomor Rekening Pinjaman
    #When I input Nominal Setor Pinjaman
    #And I input password
    #Then Transaction Setor Pinjaman success <feature>
  #	Then Transaction Receipt Sent to SMS	
#
    #Examples: 
      #| case | feature |
      #|    1 | Setor Pinjaman |
      #
   #@tag13-TC013-TrxSuccessSetorPinjamanConvertPDF
  #Scenario Outline: Setor Pinjaman Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Pinjaman
    #When I input Nomor Rekening Pinjaman
    #When I input Nominal Setor Pinjaman
    #And I input password
    #Then Transaction Setor Pinjaman success <feature>
  #	Then Transaction Receipt Save to PDF
#
    #Examples: 
      #| case | feature |
      #|    1 | Setor Pinjaman |