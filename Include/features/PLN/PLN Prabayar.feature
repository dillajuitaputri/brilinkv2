#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  
  @tag1-TC101-PembayaranPrabayarPLNDataValid
  Scenario Outline: User melakukan transaksi data yang valid
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu PLN
    When I choose Jenis Transaksi PLN
    When I input IDPEL atau Nomor Meteran
    And I click button KIRIM
    When I choose Nominal
    And I input password
    When Get data PLN transaction success
    Then Transaction PLN success <feature>
 
    Examples: 
      | case | feature |
      |    1 | PLN Prabayar |

  #@tag2-TC102-PembayaranPrabayarPLNInValid
  #Scenario Outline: User memasukan format nomor meteran tidak benar
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IDPEL atau Nomor Meteran
    #And I click button KIRIM
 #
    #Examples: 
      #| case |
      #|    2 |

  #@tag3-TC103-InputNomorMeteranKurangDari12Digit
  #Scenario Outline: User memasukan nomor meteran kurang dari 12 digit
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IDPEL atau Nomor Meteran
    #And I click button KIRIM
 #
    #Examples: 
      #| case |
      #|    3 |
      
  #@tag4-TC104-UserTidakInputNoMeteran
  #Scenario Outline: User tidak input IDPEL atau Nomor Meteran
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #And I click button KIRIM
 #
    #Examples: 
      #| case |
      #|    3 |
      
  #@tag5-TC105-SalahInputPassword
  #Scenario Outline: User salah memasukan password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IDPEL atau Nomor Meteran
    #And I click button KIRIM
    #When I choose Nominal
    #And I input wrong password
    #
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag6-TC106-SalahInputPasswordTigaKali
  #Scenario Outline: User salah memasukkan password transaksi sebanyak 3 kali
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IDPEL atau Nomor Meteran
    #And I click button KIRIM
    #When I choose Nominal
    #And I input wrong password
#		Then I tried to enter the wrong password three times
#		Then I tried to enter the wrong password three times
    #
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag7-TC107-SessionHabis
  #Scenario Outline: User melakukan transaksi PLN Prabayar lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IDPEL atau Nomor Meteran
    #And I click button KIRIM
    #When I choose Nominal
    #And Session over
#
    #Examples: 
      #| case |
      #|    1 |
      #
  #@tag8-TC108-KoneksiTerputus
  #Scenario Outline: User melakukan transaksi PLN Prabayar lalu koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IDPEL atau Nomor Meteran
    #And I click button KIRIM
    #When I choose Nominal
    #When Connection off
#
    #Examples: 
      #| case |
      #|    1 |
      
   #@tag9-TC109-TrxSuccessPLNPrabayarSentToEmail
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IDPEL atau Nomor Meteran
    #And I click button KIRIM
    #When I choose Nominal
    #And I input password
 #		Then Transaction Receipt Sent to Email
 #
    #Examples: 
      #| case |
      #|    1 |
      #
   #@tag10-TC110-TrxSuccessPLNPrabayarSentToSMS
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IDPEL atau Nomor Meteran
    #And I click button KIRIM
    #When I choose Nominal
    #And I input password
    #Then Transaction Receipt Sent to SMS
 #
    #Examples: 
      #| case |
      #|    1 |
      #
   #@tag11-TC111-TrxSuccessPLNPrabayarConvertPDF
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IDPEL atau Nomor Meteran
    #And I click button KIRIM
    #When I choose Nominal
    #And I input password
 #		Then Transaction Receipt Save to PDF
 #
    #Examples: 
      #| case |
      #|    1 |