#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag1-TC201-PembayaranPascabayarPLNDataValid
  Scenario Outline: PLN Pascabayar Payment
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu PLN
    When I choose Jenis Transaksi PLN
    When I input IPDEL
    And I click button KIRIM
    And I input password
    When Get data PLN transaction success
    Then Transaction PLN success <feature>
  

    Examples: 
      | case | feature |
      |    5 | PLN Pascabayar |
      
  #@tag2-TC202-PembayaranPascabayarPLNInValid
  #Scenario Outline: User memasukan format nomor meteran tidak benar
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IPDEL
    #And I click button KIRIM
#
    #Examples: 
      #| case |
      #|    6 |
      
  #@tag3-TC203-InputNomorMeteranKurangDari12Digit
  #Scenario Outline: User memasukan nomor meteran kurang dari 12 digit
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IPDEL
    #And I click button KIRIM
#
    #Examples: 
      #| case |
      #|    7 |
      
  #@tag4-TC204-UserTidakInputIPDEL
  #Scenario Outline: User tidak input IDPEL
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #And I click button KIRIM
#
#
    #Examples: 
      #| case |
      #|    5 |
      #
  
  #@tag5-TC205-SalahInputPassword
  #Scenario Outline: PLN Pascabayar Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IPDEL
    #And I click button KIRIM
   #	And I input wrong password
#
    #Examples: 
      #| case |
      #|    5 |
      #
  #@tag6-TC206-SalahInputPasswordTigaKali
  #Scenario Outline: PLN Pascabayar Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IPDEL
    #And I click button KIRIM
   #	And I input wrong password
   #	Then I tried to enter the wrong password three times
#		Then I tried to enter the wrong password three times
#
    #Examples: 
      #| case |
      #|    5 |
      
      
  #@tag7-TC207-SessionHabis
  #Scenario Outline:  User melakukan transaksi PLN Pascabayar lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IPDEL
    #And I click button KIRIM
    #And Session over   
#
    #Examples: 
      #| case |
      #|    5 |
      #
  #@tag8-TC208-KoneksiTerputus
  #Scenario Outline:  User melakukan transaksi Pascabayar lalu koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IPDEL
    #And I click button KIRIM
    #When Connection off  
#
    #Examples: 
      #| case |
      #|    5 |
      
   #@tag9-TC209-TrxSuccessPLNPascabayarSentToEmail
  #Scenario Outline: PLN Pascabayar Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IPDEL
    #And I click button KIRIM
    #And I input password
    #Then Transaction PLN Pascabayar success <feature>
    #Then Transaction Receipt Sent to Email
#
    #Examples: 
      #| case | feature |
      #|    5 | PLN Pascabayar |
      #
   #@tag10-TC210-TrxSuccessPLNPascabayarSentToSMS
  #Scenario Outline: PLN Pascabayar Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IPDEL
    #And I click button KIRIM
    #And I input password
    #Then Transaction PLN Pascabayar success <feature>
    #Then Transaction Receipt Sent to SMS	
#
    #Examples: 
      #| case | feature |
      #|    5 | PLN Pascabayar |
      #
   #@tag11-TC211-TrxSuccessPLNPascabayarConvertPDF
  #Scenario Outline: PLN Pascabayar Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input IPDEL
    #And I click button KIRIM
    #And I input password
    #Then Transaction PLN Pascabayar success <feature>
    #Then Transaction Receipt Save to PDF
#
    #Examples: 
      #| case | feature |
      #|    5 | PLN Pascabayar |