#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

   @tag1-301-PembayaranNonTagihanListrikValid
  Scenario Outline: User melakukan transaksi data yang valid
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu PLN
    When I choose Jenis Transaksi PLN
    When I input Nomor Registrasi
    And I click button KIRIM
    And I input password
    When Get data PLN transaction success
    Then Transaction PLN success <feature>
   
    Examples: 
      | case | feature |
      |    9 | PLN Non Tagihan Listrik |
      
  #@tag2-TC302-PembayaranNonTagihanListrikPLNInValid
  #Scenario Outline: PembayaranNonTagihanListrikPLNInValid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Registrasi
    #And I click button KIRIM
#
    #Examples: 
      #| case |
      #|    10 |
      
  #@tag3-TC303-InputNomorRegistrasiKurangDari13Digit
  #Scenario Outline: User memasukan nomor registrasi kurang dari 13 digit
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Registrasi
    #And I click button KIRIM
#
    #Examples: 
      #| case |
      #|    11 |
      
  #@tag4-TC304-UserTidakInputNoRegistrasi
  #Scenario Outline: User tidak input Nomor Registrasi
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #And I click button KIRIM
#
    #Examples: 
      #| case |
      #|    11 |
      
  #@tag5-TC305-SalahInputPassword
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Registrasi
    #And I click button KIRIM
  #	And I input wrong password
#
    #Examples: 
      #| case |
      #|    9 |
      #
  #@tag6-TC306-SalahInputPasswordTigaKali
  #Scenario Outline: User salah memasukkan password transaksi sebanyak 3 kali
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Registrasi
    #And I click button KIRIM
  #	And I input wrong password
  #	Then I tried to enter the wrong password three times
#		Then I tried to enter the wrong password three times
    #
#
    #Examples: 
      #| case |
      #|    9 |
      
  #@tag7-701-SessionHabis
  #Scenario Outline: User melakukan transaksi Non Tagihan Listrik lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Registrasi
    #And I click button KIRIM
    #And Session over   
#
    #Examples: 
      #| case |
      #|    9 |
      
  #@tag8-308-KoneksiTerputus
  #Scenario Outline: User melakukan transaksi Non Tagihan Listrik lalu koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Registrasi
    #And I click button KIRIM
   #	When Connection off
#
    #Examples: 
      #| case |
      #|    9 |
      
   #@tag9-309-TrxSuccessPLNNonTagLisSentToEmail
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Registrasi
    #And I click button KIRIM
    #And I input password
    #Then Transaction PLN Non Tagihan Listrik success <feature>
    #Then Transaction Receipt Sent to Email
#
    #Examples: 
      #| case | feature |
      #|    9 | PLN Non Tagihan Listrik |
      #
   #@tag10-310-TrxSuccessPLNNonTagLisSentToSMS
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Registrasi
    #And I click button KIRIM
    #And I input password
    #Then Transaction PLN Non Tagihan Listrik success <feature>
    #Then Transaction Receipt Sent to SMS	
#
    #Examples: 
      #| case | feature |
      #|    9 | PLN Non Tagihan Listrik |
      #
    #@tag11-311-TrxSuccessPLNNonTagLisConvertPDF
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Registrasi
    #And I click button KIRIM
    #And I input password
    #Then Transaction PLN Non Tagihan Listrik success <feature>
    #Then Transaction Receipt Save to PDF
#
    #Examples: 
      #| case | feature |
      #|    9 | PLN Non Tagihan Listrik |