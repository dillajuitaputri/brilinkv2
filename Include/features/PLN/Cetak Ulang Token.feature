#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag1-TC401-CetakUlangTokenDataValid
  Scenario Outline: User melakukan transaksi data yang valid
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu PLN
    When I choose Jenis Transaksi PLN
    When I input Nomor Meteran
    When I input Nomor Referensi
    And I click button KIRIM
    When Get data PLN transaction success
    Then Transaction PLN success <feature>
    Examples: 
      | case |
      |    13 |
      
  #@tag2-TC402-CetakUlangTokenDataNomorMeteranInValid
  #Scenario Outline: User memasukan format nomor meteran tidak benar
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Meteran
    #When I input Nomor Referensi
    #And I click button KIRIM
#
    #Examples: 
      #| case |
      #|    14 |
      #
      
  #@tag3-TC403-CetakUlangTokenDataNomorReferensiInValid
  #Scenario Outline: User memasukan format nomor referensi tidak benar
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Meteran
    #When I input Nomor Referensi
    #And I click button KIRIM
#
    #Examples: 
      #| case |
      #|    15 |
      
  #@tag4-TC404-UserTidakInputNoMeteran
  #Scenario Outline: User tidak input nomor meteran
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Referensi
    #And I click button KIRIM
#
    #Examples: 
      #| case |
      #|    13 |
      
  #@tag5-TC405-UserTidakInputNoReferensi
  #Scenario Outline: User tidak input nomor referensi
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Meteran
    #And I click button KIRIM
#
    #Examples: 
      #| case |
      #|    13 |
      
   #@tag6-TC406-SalahInputPassword
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Meteran
    #When I input Nomor Referensi
    #And I click button KIRIM
    #When I choose Nominal
    #And I input wrong password
#
    #Examples: 
      #| case |
      #|    13 |
      #
  #@tag6-TC406-SalahInputPasswordTigaKali
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Meteran
    #When I input Nomor Referensi
    #And I click button KIRIM
    #When I choose Nominal
    #And I input wrong password
    #Then I tried to enter the wrong password three times
#		Then I tried to enter the wrong password three times
#
    #Examples: 
      #| case |
      #|    13 |
      
  #@tag7-TC407-SessionHabis
  #Scenario Outline: User melakukan transaksi Cetak Ulang Token lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Meteran
    #When I input Nomor Referensi
    #And I click button KIRIM
    #When I choose Nominal
    #And Session over   
#
    #Examples: 
      #| case |
      #|    13 |
      
  #@tag8-TC408-KoneksiTerputus
  #Scenario Outline: User melakukan transaksi Cetak Ulang Token lalu koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Meteran
    #When I input Nomor Referensi
    #And I click button KIRIM
    #When I choose Nominal
    #When Connection off
 
    #Examples: 
      #| case |
      #|    13 |
      #
    #@tag9-TC409-CetakUlangTokenDataValidSentToEmail
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Meteran
    #When I input Nomor Referensi
    #And I click button KIRIM
    #When I choose Nominal
    #And I input password
    #Then Transaction PLN Non Tagihan Listrik success <feature>
    #Then Transaction Receipt Sent to Email
#
    #Examples: 
      #| case |
      #|    13 |
      #
    #@tag10-TC410-CetakUlangTokenDataValidSentToSMS
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Meteran
    #When I input Nomor Referensi
    #And I click button KIRIM
    #When I choose Nominal
    #And I input password
    #Then Transaction PLN Non Tagihan Listrik success <feature>
    #Then Transaction Receipt Sent to SMS
#
    #Examples: 
      #| case |
      #|    13 |
      #
    #@tag11-TC411-CetakUlangTokenDataValidConvertPDF
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PLN
    #When I choose Jenis Transaksi PLN
    #When I input Nomor Meteran
    #When I input Nomor Referensi
    #And I click button KIRIM
    #When I choose Nominal
    #And I input password
    #Then Transaction PLN Non Tagihan Listrik success <feature>
    #Then Transaction Receipt Save to PDF
#
    #Examples: 
      #| case |
      #|    13 |
      
 