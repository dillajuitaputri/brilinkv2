#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Feature BAZNAS Register
  I want to use this template for my feature file

  @tag1-TC201-Email-KodePembayaranValid
  Scenario Outline: User melakukan Registrasi dengan data email yang valid
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu BAZNAS
    When I choose Registrasi
    When I choose ID Registrasi
    When I input NIK
    When I input Nama
    When I input Alamat
    And I input password
    #Then Action struk

    Examples: 
      | case |
      |    1 |
      
      
  #@tag2-TC202-Email-EmailInvalid
  #Scenario Outline: User memasukan Email dengan data yang tidak valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Registrasi
    #When I choose ID Registrasi
    #When I input NIK
    #When I input Nama
    #When I input Alamat
#
    #Examples: 
      #| case |
      #|    2 |
      
  #@tag3-TC203-Email-NIKInvalid
  #Scenario Outline: User memasukan NIK dengan data yang tidak valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Registrasi
    #When I choose ID Registrasi
    #When I input NIK
    #When I input Nama
    #When I input Alamat
#
    #Examples: 
      #| case |
      #|    3 |
      #
      
  #@tag4-TC204-UserTidakInputNama
  #Scenario Outline: User tidak input nama
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Registrasi
    #When I choose ID Registrasi
    #When I input NIK
    #When I input Alamat
#
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag5-TC205-UserTidakInputNIK
  #Scenario Outline: User melakukan Registrasi dengan data email yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Registrasi
    #When I choose ID Registrasi
    #When I input Nama
    #When I input Alamat
    #And I input password
 #
#
    #Examples: 
      #| case |
      #|    1 |
      
      
  #@tag6-TC206-SessionHabis
  #Scenario Outline: User melakukan transaksi Register BAZNAS lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Registrasi
    #When I choose ID Registrasi
    #When I input NIK
    #When I input Nama
    #When I input Alamat
#		And Session over 
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag7-TC207-KoneksiTerputus
  #Scenario Outline: User melakukan transaksi Register BAZNAS lalu koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Registrasi
    #When I choose ID Registrasi
    #When I input NIK
    #When I input Nama
    #When I input Alamat
#		And Session over 
#
    #Examples: 
      #| case |
      #|    1 |