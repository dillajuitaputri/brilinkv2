#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Feature BAZNAS Pembayaran
  I want to use this template for my feature file

  @tag1-TC001-KodePembayaranValid
  Scenario Outline: User memasukan Kode Pembayaran dengan data yang valid
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu BAZNAS
    When I choose Pembayaran
    When I choose ID Pembayaran
    When I choose Tipe Pembayaran
    When I input Nominal
    And I input password 
    When Get data BAZNAS Pembayaran transaction success   
    Then Transcation BAZNAS Pembayaran success <feature>

    Examples: 
      | case | feature |
      |    1 | BAZNAS Pembayaran |
      
      
  #@tag2-TC002-NomorHandphoneInValid
  #Scenario Outline: User memasukan nomor handphone dengan data yang tidak valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Pembayaran
    #When I choose ID Pembayaran
    #When I choose Tipe Pembayaran
    #When I input Nominal
#
    #Examples: 
      #| case |
      #|    2 |
        
      
  #@tag3-TC003-InputNominalInvalid
  #Scenario Outline: User memasukan Nominal tidak valid / di bawah minimal 25.000
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Pembayaran
    #When I choose ID Pembayaran
    #When I choose Tipe Pembayaran
    #When I input Nominal
    #
    #Examples: 
      #| case |
      #|    3 |
      
  #@tag4-TC004-TidakInputNomorHandphone
  #Scenario Outline: User tidak mengisi Nomor Handphone
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Pembayaran
    #When I choose Tipe Pembayaran
    #When I input Nominal
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag5-TC005-TidakInputNominal
  #Scenario Outline: User tidak mengisi Nominal
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Pembayaran
    #When I choose ID Pembayaran
    #When I choose Tipe Pembayaran
#		And I click button KIRIM
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag6-TC006-SalahInputPassword
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Pembayaran
    #When I choose ID Pembayaran
    #When I choose Tipe Pembayaran
    #When I input Nominal
#		And I input wrong password
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag7-TC007-SalahInputPasswordTigaKali
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Pembayaran
    #When I choose ID Pembayaran
    #When I choose Tipe Pembayaran
    #When I input Nominal
#		And I input wrong password
#		Then I tried to enter the wrong password three times
#		Then I tried to enter the wrong password three times
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag8-TC008-SessionHabis
  #Scenario Outline: User melakukan transaksi Pembayaran BAZNAS lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Pembayaran
    #When I choose ID Pembayaran
    #When I choose Tipe Pembayaran
    #When I input Nominal
#		And Session over
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag9-TC009-KoneksiTerputus
  #Scenario Outline: User memasukan Kode Pembayaran dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu BAZNAS
    #When I choose Pembayaran
    #When I choose ID Pembayaran
    #When I choose Tipe Pembayaran
    #When I input Nominal
#		When Connection off
#
    #Examples: 
      #| case |
      #|    1 |
      #
      