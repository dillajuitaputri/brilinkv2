#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  	@tag1
  	Scenario Outline: Nomor kartu dan tanggal expire benar
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I click menu Info Saldo
    When I input nomor kartu ATM
    And I choose nomor handphone
     #And I input password
    #Then Transaction Info Saldo success <feature>
    Examples: 
      | case |    feature   |
      |    1 |  Info Saldo  |
      
  #	@tag2
  #	Scenario Outline: Nomor kartu benar tetapi tanggal expire salah
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Info Saldo
    #When I input nomor kartu ATM
    #Examples: 
      #| case |
      #|    2 |
      
  #	@tag3
  #	Scenario Outline: Nomor kartu benar tetapi tanggal expire salah
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Info Saldo
    #When I input nomor kartu ATM
    #Examples: 
      #| case |
      #|    3 |
      
    #@tag4
  #	Scenario Outline: Nomor kartu dan tanggal expire salah
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Info Saldo
    #When I input nomor kartu ATM
    #And I choose nomor handphone
    #Examples: 
      #| case |
      #|    4 |
      
    #@tag5
  #	Scenario Outline: Nomor kartu kurang dari 16 digit
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Info Saldo
    #When I input nomor kartu ATM
    #Examples: 
      #| case |
      #|    5 |
      
    #@tag6
  #	Scenario Outline: Kode OTP benar
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Info Saldo
    #When I input nomor kartu ATM
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    6 |
      
    #@tag7
  #	Scenario Outline: Kode OTP Salah
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Info Saldo
    #When I input nomor kartu ATM
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    7 |
      
    #@tag8
  #	Scenario Outline: Terlalu lama memasukkan kode OTP
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Info Saldo
    #When I input nomor kartu ATM
    #And I choose nomor handphone
    #Examples: 
      #| case |
      #|    8 |
      
    #@tag9
  #	Scenario Outline: Salah memasukkan password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Info Saldo
    #When I input nomor kartu ATM
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    9 |
      
    #@tag10
  #	Scenario Outline: Salah memasukkan password sebanyak 3x
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Info Saldo
    #When I input nomor kartu ATM
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    10 |
      
    #@tag11
  #	Scenario Outline: Tidak mengisi tanggal expired
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Info Saldo
    #When I input nomor kartu ATM
    #Examples: 
      #| case |
      #|    11 |
      
    #@tag12
  #	Scenario Outline: Session Habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Info Saldo
    #When I input nomor kartu ATM
    #And Session over before OTP
    #Examples: 
      #| case |
      #|    12|
      
    #@tag13
  #	Scenario Outline: Koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Info Saldo
    #When I input nomor kartu ATM
    #And I choose nomor handphone
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    13|