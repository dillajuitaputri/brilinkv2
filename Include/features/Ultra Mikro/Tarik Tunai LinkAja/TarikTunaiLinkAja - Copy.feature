#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

   @TariktunaiLinkAja
  Scenario Outline: Nomor token LinkAja secara manual
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I click menu Ultra Mikro
    When I choose submenu
     #And I input password
    #Then Action struk
    Examples: 
      | case |
      |    1 |
      
        #@TariktunaiLinkAja
  #Scenario Outline: Nomor token invalid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    2 |
      #
        #@TariktunaiLinkAja
  #Scenario Outline: Nominal tarik tunai melebihi limit harian
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
   # When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    3 |
      #
        #@TariktunaiLinkAja
  #Scenario Outline: Salah memasukkan password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    4 |
      #
        #@TariktunaiLinkAja
  #Scenario Outline: Salah memasukkan password sebanyak 3x
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    5 |
      #
        #@TariktunaiLinkAja
  #Scenario Outline: Session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And Sesssion over
    #Then Action struk
    #Examples: 
      #| case |
      #|    6 |
      #
        #@TariktunaiLinkAja
  #Scenario Outline: Koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    7 |