#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @PegadaianRefferalPinjaman
  Scenario Outline: Nominal pinjaman refferal melebihi limit harian 
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I click menu Ultra Mikro
    When I choose submenu
     #And I input password
    #Then Action struk
    Examples: 
      | case |
      |    1 |
      
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Nama nasabah tidak sesuai dengan nama di KTP
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Nama nasabah menggunakan numerik
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Nama nasabah lebih dari 20 karakter  
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Memasukkan nama panggilan saja  
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Nomor handphone kurang dari 12 digit
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Nomor handphone sudah tidak aktif
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Nomor KTP invalid  
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Memasukkan nomor SIM (bukan KTP) 
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Nomor KTP kurang dari 16 digit  
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Nama dan no KTP tidak sama
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Tanggal lahir berbeda dengan yang tercantum di KTP
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Tahun lahir kurang dari 18 tahun 
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Keterangan dengan kombinasi numerik dan alphabetic
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Tidak mengisi kolom keterangan 
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: No Polisi dengan format invalid 
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Tidak mengisi kolom tahun perakitan kendaraan 
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline:  
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Salah memasukkan password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Salah memasukkan password sebanyak 3x
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Session habis 
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |
      #
      #@PegadaianRefferalPinjaman
  #Scenario Outline: Koneksi terputus 
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Ultra Mikro
    #When I choose submenu
     #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |