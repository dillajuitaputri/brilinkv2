#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

    @tag1-LinkAjaWithdataValid
  Scenario Outline: LinkAja Payment
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I click menu Top Up LinkAja
    When I input Nomor Top Up LinkAja
    When I input Nominal Pembayaran LinkAja
    And I input password
    When Get data LinkAja transaction success
    Then Transcation Top Up Link Aja success <feature>

    Examples: 
      | case | feature |
      |    1 | Link Aja |
      
      
  #@tag2-TC002-NomorHandphoneInValid
  #Scenario Outline: User memasukan nomor Linkaja dengan data yang tidak valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up LinkAja
    #When I input Nomor Top Up LinkAja
#
    #Examples: 
      #| case |
      #|    2 |
      
      
  #@tag3-TC003-SaldoTidakCukup
  #Scenario Outline: User memasukan nominal melebihi saldo 
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up LinkAja
    #When I input Nomor Top Up LinkAja
    #When I input Nominal Pembayaran LinkAja
    #And I input password
  #
    #Examples: 
      #| case |
      #|    3 |
      #
  #@tag4-TC004-SalahInputPassword
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up LinkAja
    #When I input Nomor Top Up LinkAja
    #When I input Nominal Pembayaran LinkAja
     #And I input wrong password
     #
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag5-TC005-SalahInputPasswordTigaKali
  #Scenario Outline: User salah memasukkan password transaksi sebanyak 3 kali
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up LinkAja
    #When I input Nomor Top Up LinkAja
    #When I input Nominal Pembayaran LinkAja
    #And I input wrong password
    #Then I tried to enter the wrong password three times
#		Then I tried to enter the wrong password three times
     #
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag6-TC006-SessionHabis
  #Scenario Outline: User melakukan transaksi LinkAja lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up LinkAja
    #When I input Nomor Top Up LinkAja
    #When I input Nominal Pembayaran LinkAja
    #And Session over  
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag7-TC007-KoneksiTerputus
  #Scenario Outline: User melakukan transaksi LinkAja lalu koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up LinkAja
    #When I input Nomor Top Up LinkAja
    #When I input Nominal Pembayaran LinkAja
    #When Connection off
#
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag8-LinkAjaSentToEmail
  #Scenario Outline: LinkAja Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up LinkAja
    #When I input Nomor Top Up LinkAja
    #When I input Nominal Pembayaran LinkAja
    #And I input password
    #Then Transcation Top Up Link Aja success <feature>
    #Then Transaction Receipt Sent to Email
    #
    #Examples: 
      #| case | feature |
      #|    1 | Link Aja |
      #
  #@tag9-LinkAjaSentToSMS
  #Scenario Outline: LinkAja Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up LinkAja
    #When I input Nomor Top Up LinkAja
    #When I input Nominal Pembayaran LinkAja
    #And I input password
    #Then Transcation Top Up Link Aja success <feature>
    #Then Transaction Receipt Sent to SMS
#
    #
#
    #Examples: 
      #| case | feature |
      #|    1 | Link Aja |
      
  #@tag10-LinkAjaConvertToPDF
  #Scenario Outline: LinkAja Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up LinkAja
    #When I input Nomor Top Up LinkAja
    #When I input Nominal Pembayaran LinkAja
    #And I input password
    #Then Transcation Top Up Link Aja success <feature>
    #Then Transaction Receipt Save to PDF
    #
#
    #Examples: 
      #| case | feature |
      #|    1 | Link Aja |