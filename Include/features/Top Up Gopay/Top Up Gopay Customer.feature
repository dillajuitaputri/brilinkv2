#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

   @tag1-TC101-TopUpGopayDataNormal
  Scenario Outline: TopUpGopayDataNormal
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I click menu Top Up Gopay
    When I choose jenis transaksi Gopay
    When I input Nomor dan Nominal Top Up Gopay
    And I input password
    When Get data Gopay transaction success
    Then Transaction Gopay success <feature>

    Examples: 
      | case | feature |
      |    1 | Top Up Gopay Customer |
      
  #@tag2-TC102-TopUpGopayDataInvalid
  #Scenario Outline: BRIVA Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up Gopay
    #When I choose jenis transaksi Gopay
    #When I input Nomor dan Nominal Top Up Gopay
#
    #Examples: 
      #| case | 
      #|    2 | 
      
      
  #@tag3-TC103-SaldoTidakCukup
  #Scenario Outline: BRIVA Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up Gopay
    #When I choose jenis transaksi Gopay
    #When I input Nomor dan Nominal Top Up Gopay
    #And I input password
#
    #Examples: 
      #| case | 
      #|    3 | 
      
  #@tag4-TC004-SalahInputPassword
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up Gopay
    #When I choose jenis transaksi Gopay
    #When I input Nomor dan Nominal Top Up Gopay
    #And I input wrong password
#
    #Examples: 
      #| case | 
      #|    1 | 
      
  #@tag5-TC005-SalahInputPasswordTigaKali
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up Gopay
    #When I choose jenis transaksi Gopay
    #When I input Nomor dan Nominal Top Up Gopay
    #And I input wrong password
    #Then I tried to enter the wrong password three times
    #Then I tried to enter the wrong password three times
#
    #Examples: 
      #| case | 
      #|    1 | 
      
  #@tag6-TC006-SessionHabis
  #Scenario Outline: User melakukan transaksi Gopay lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up Gopay
    #When I choose jenis transaksi Gopay
    #When I input Nomor dan Nominal Top Up Gopay
#		And Session over 
#
    #Examples: 
      #| case | 
      #|    1 | 
      
  #@tag7-TC007-KoneksiTerputus
  #Scenario Outline: User melakukan transaksi Gopay lalu koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up Gopay
    #When I choose jenis transaksi Gopay
    #When I input Nomor dan Nominal Top Up Gopay
    #When Connection off
#
    #Examples: 
      #| case | 
      #|    1 | 
      
  #@tag8-TC108-TrxSuccessTopUpGopayCustomerSentToEmail
  #Scenario Outline: BRIVA Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up Gopay
    #When I choose jenis transaksi Gopay
    #When I input Nomor dan Nominal Top Up Gopay
    #And I input password
    #Then Transaction Gopay success <feature>
#		Then Transaction Receipt Sent to Email
#
    #Examples: 
      #| case | feature |
      #|    1 | Customer |
      #
  #@tag9-TC109-TrxSuccessTopUpGopayCustomerSentToSMS
  #Scenario Outline: BRIVA Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up Gopay
    #When I choose jenis transaksi Gopay
    #When I input Nomor dan Nominal Top Up Gopay
    #And I input password
    #Then Transaction Gopay success <feature>
#		Then Transaction Receipt Sent to SMS	
#			
    #Examples: 
      #| case | feature |
      #|    1 | Customer |
      #
  #@tag10-TC110-TrxSuccessTopUpGopayCustomerConvertPDF
  #Scenario Outline: BRIVA Payment
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Top Up Gopay
    #When I choose jenis transaksi Gopay
    #When I input Nomor dan Nominal Top Up Gopay
    #And I input password
    #Then Transaction Gopay success <feature>
    #Then Transaction Receipt Save to PDF
#
    #Examples: 
      #| case | feature |
      #|    1 | Customer |