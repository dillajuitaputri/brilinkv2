#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  
   @tag1-TC001-KodePembayaranValid
  Scenario Outline: User memasukan Kode Pembayaran dengan data yang valid
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu Tokopedia
    When I input Kode Pembayaran Tokopedia
    And I input password
    When Get data Tokopedia transaction success
    Then Transaction Tokopedia success <feature>

    Examples: 
      | case | feature |
      |    1 | Tokopedia |
      
      
#[prepare]belum ada validasi nya di brilink      
    #@tag2-TC002-KodePembayaranInValid
  #Scenario Outline: User memasukan Kode Pembayaran dengan data yang tidak valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Tokopedia
    #When I input Kode Pembayaran Tokopedia
   #
    #Examples: 
      #| case | feature |
      #|    2 | Tokopedia |
   
 
#case menggunakan akun yang tidak ada saldo / va tagihan besar melebihi  
    #@tag3-TC003-SaldoTidakCukup
  #Scenario Outline: User melakukan transaksi Tokopedia dengan kondisi saldo tidak cukup
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Tokopedia
    #When I input Kode Pembayaran Tokopedia
    #And I input password
    #Then Transaction success <feature>
    #Then Action struk
#
    #Examples: 
      #| case | feature |
      #|    3 | Tokopedia |
      
    #@tag4-TC004-SalahInputPassword
  #Scenario Outline: User salah input password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Tokopedia
    #When I input Kode Pembayaran Tokopedia
    #And I input wrong password
#
    #Examples: 
      #| case | feature |
      #|    1 | Tokopedia |
      
    #@tag5-TC005-SalahInputPasswordTigaKali
  #Scenario Outline: User salah memasukkan password transaksi sebanyak 3 kali
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Tokopedia
    #When I input Kode Pembayaran Tokopedia
    #And I input wrong password
#		Then I tried to enter the wrong password three times
#		Then I tried to enter the wrong password three times
    #Examples: 
      #| case | feature |
      #|    1 | Tokopedia |
   
   #@tag6-TC006-SessionHabis
  #Scenario Outline: User melakukan transaksi Tokopedia lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Tokopedia
    #When I input Kode Pembayaran Tokopedia
    #And Session over      
#
    #Examples: 
      #| case | feature |
      #|    1 | Tokopedia |   
   
      
  #@tag7-TC007-KoneksiTerputus
  #Scenario Outline: User melakukan transaksi Tokopedia lalu koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Tokopedia
    #When I input Kode Pembayaran Tokopedia
    #When Connection off
#
    #Examples: 
      #| case | feature |
      #|    1 | Tokopedia |
      
   #@tag8-TC108-TrxSuccessTokopediaSentToEmail
  #Scenario Outline: User memasukan Kode Pembayaran dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Tokopedia
    #When I input Kode Pembayaran Tokopedia
    #And I input password
    #Then Transaction Tokopedia success <feature>
    #Then Transaction Receipt Sent to Email
#
    #Examples: 
      #| case | feature |
      #|    1 | Tokopedia |
      #
   #@tag9-TC009-TrxSuccessTokopediaSentToSMS
  #Scenario Outline: User memasukan Kode Pembayaran dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Tokopedia
    #When I input Kode Pembayaran Tokopedia
    #And I input password
    #Then Transaction Tokopedia success <feature>
    #Then Transaction Receipt Sent to SMS
#
    #Examples: 
      #| case | feature |
      #|    1 | Tokopedia |
      #
   #@tag10-TC010-TrxSuccessTokopediaConvertToPDF
  #Scenario Outline: User memasukan Kode Pembayaran dengan data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Tokopedia
    #When I input Kode Pembayaran Tokopedia
    #And I input password
    #Then Transaction Tokopedia success <feature>
    #Then Transaction Receipt Save to PDF
#
    #Examples: 
      #| case | feature |
      #|    1 | Tokopedia |