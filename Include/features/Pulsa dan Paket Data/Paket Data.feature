#Author: dillajuitaputri@gmail.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

   @tag1
  Scenario Outline: Pembelian paket dari input nomor manual
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I click menu Paket Data
    When I input Nomor Paket Data
    When I choose Paket Data
    And I input password
    Then Need help or go to catatan aktivitas paket data
    Then Transaction Paket Data dan Pulsa success <feature>
    #Then Action struk
    Examples: 
      | case | feature |
      |    1 | Paket Data |
      
   #@tag2
  #Scenario Outline: Pembelian paket dari phone book
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Paket Data
    #When I input Nomor Paket Data
    #When I choose Paket Data
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    2 |
      #
   #@tag3
  #Scenario Outline: Cari nomor dari phone book
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Paket Data
    #When I input Nomor Paket Data
    #When I choose Paket Data
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    3 |
      #
   #@tag4
  #Scenario Outline: Pilih salah satu paket data
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Paket Data
    #When I input Nomor Paket Data
    #When I choose Paket Data
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    4 |
      #
   #@tag5
  #Scenario Outline: Salah memasukkan password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Paket Data
    #When I input Nomor Paket Data
    #When I choose Paket Data
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    5 |
      #
   #@tag6
  #Scenario Outline: Salah memasukkan password sebanyak 3x
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Paket Data
    #When I input Nomor Paket Data
    #When I choose Paket Data
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    6 |
      #
   #@tag7
  #Scenario Outline: Session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Paket Data
    #When I input Nomor Paket Data
    #When I choose Paket Data
    #And Session over
    #Then Action struk
    #Examples: 
      #| case |
      #|    7 |
      #
   #@tag8
  #Scenario Outline: Koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Paket Data
    #When I input Nomor Paket Data
    #When I choose Paket Data
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |