#Author: dillajuitaputri@gmail.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

 	@tag1
  Scenario Outline: Briva
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I click menu Briva
    When I input Nomor Pembayaran Briva
    When I input Nominal Pembayaran Briva
    And I input password
    #Then Need help
    Then Transaction Briva success <feature>
    Examples: 
      | case | feature |
      |    1 |  Briva  |
      
  #@tag2
  #Scenario Outline: Kode pembayaran briva tidak terdaftar
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Briva
    #When I input Nomor Pembayaran Briva
    #Examples: 
      #| case |
      #|    2 |
      
  #@tag3
  #Scenario Outline: Nominal yang dibayarkan berbeda dengan nominal tagihan
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Briva
    #When I input Nomor Pembayaran Briva
    #When I input Nominal Pembayaran Briva
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    3 |
       
  #@tag4
  #Scenario Outline: Salah memasukkan password transaksi
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Briva
    #When I input Nomor Pembayaran Briva
    #When I input Nominal Pembayaran Briva
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    4 |
      
  #@tag5
  #Scenario Outline: Salah memasukkan password transaksi sebanyak 3x
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Briva
    #When I input Nomor Pembayaran Briva
    #When I input Nominal Pembayaran Briva
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    5 |
      
  #@tag6
  #Scenario Outline: Session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Briva
    #When I input Nomor Pembayaran Briva
    #When I input Nominal Pembayaran Briva
    #And Session over
    #Then Action struk
    #Examples: 
      #| case |
      #|    6 |
      
  #@tag7
  #Scenario Outline: Koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Briva
    #When I input Nomor Pembayaran Briva
    #When I input Nominal Pembayaran Briva
    #When Connection off
    #Examples: 
      #| case |
      #|    1 |
      
  #@tag8
  #Scenario Outline:
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I click menu Briva
    #When I input Nomor Pembayaran Briva
    #When I input Nominal Pembayaran Briva
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    8 |