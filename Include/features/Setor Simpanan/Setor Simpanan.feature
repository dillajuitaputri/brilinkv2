#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Feature Setor Simpanan
  I want to use this template for my feature file

  @tag1-TC001-SetorSimpananDataValid
  Scenario Outline: User melakukan transaksi data yang valid
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu Setor Simpanan
    When I input Nomor Rekening for Setor Simpanan Payment
    When I input Jumlah Setoran
    And I click button KIRIM
    And I input password
    When Get data Setor Simpanan transaction success
    Then Transaction Setor Simpan success <feature>
   

    Examples: 
      | case | feature |
      |    1 | Setor Simpanan |
      
   #@tag2-TC002-SetorSimpananDataInValid
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Simpanan
    #When I input Nomor Rekening for Setor Simpanan Payment
    #When I input Jumlah Setoran
    #And I click button KIRIM
    #
    #Examples: 
      #| case | 
      #|    2 | 
      
  #@tag3-TC003-NoRekKurangDari15Digit
  #Scenario Outline: User memasukan rekening kurang dari 15 digit
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Simpanan
    #When I input Nomor Rekening for Setor Simpanan Payment
    #When I input Jumlah Setoran
    #And I click button KIRIM
    #
    #Examples: 
      #| case | 
      #|    3 | 
      
  #@tag4-TC004-TidakInputNorek
  #Scenario Outline: User tidak input nomor rekening
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Simpanan
    #When I input Jumlah Setoran
    #And I click button KIRIM
  #
    #Examples: 
      #| case | 
      #|    1 | 
      #
      
  #@tag5-TC005-TidakInputJumlahSetoran
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Simpanan
    #When I input Nomor Rekening for Setor Simpanan Payment
    #And I click button KIRIM
#
    #Examples: 
      #| case | 
      #|    1 | 
      
  #@tag6-TC006-SalahInputPassword
  #Scenario Outline: User salah memasukan password
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Simpanan
    #When I input Nomor Rekening for Setor Simpanan Payment
    #When I input Jumlah Setoran
    #And I click button KIRIM
  #	And I input wrong password
  #	
    #Examples: 
      #| case | 
      #|    1 | 
      
  #@tag7-TC007-SalahInputPasswordTigaKali
  #Scenario Outline: User salah memasukkan password transaksi sebanyak 3 kali
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Simpanan
    #When I input Nomor Rekening for Setor Simpanan Payment
    #When I input Jumlah Setoran
    #And I click button KIRIM
  #	And I input wrong password
  #	Then I tried to enter the wrong password three times
#		Then I tried to enter the wrong password three times
  #	
    #Examples: 
      #| case | 
      #|    1 |  
      
  #@tag8-TC008-SessionHabis
  #Scenario Outline: User melakukan transaksi Setor Simpanan lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Simpanan
    #When I input Nomor Rekening for Setor Simpanan Payment
    #When I input Jumlah Setoran
    #And I click button KIRIM
#		And Session over 
#
    #Examples: 
      #| case | 
      #|    1 | 
      
  #@tag9-TC009-KoneksiTerputus
  #Scenario Outline: User melakukan transaksi Setor Simpanan lalu session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Simpanan
    #When I input Nomor Rekening for Setor Simpanan Payment
    #When I input Jumlah Setoran
    #And I click button KIRIM
#		When Connection off
#		
    #Examples: 
      #| case | 
      #|    1 |
      
    #@tag10-TC010-TrxSuccessSetorSimpananSentToEmail
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Simpanan
    #When I input Nomor Rekening for Setor Simpanan Payment
    #When I input Jumlah Setoran
    #And I click button KIRIM
    #And I input password
    #Then Transaction Setor Simpan success <feature>
    #Then Transaction Receipt Sent to Email
#
    #Examples: 
      #| case | feature |
      #|    1 | Setor Simpanan | 
      #
   #@tag11-TC011-TrxSuccessSetorSimpananSentToSMS
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Simpanan
    #When I input Nomor Rekening for Setor Simpanan Payment
    #When I input Jumlah Setoran
    #And I click button KIRIM
    #And I input password
    #Then Transaction Setor Simpan success <feature>
    #Then Transaction Receipt Sent to SMS
#
    #Examples: 
      #| case | feature |
      #|    1 | Setor Simpanan |
      #
   #@tag12-TC012-TrxSuccessSetorSimpananConvertPDF
  #Scenario Outline: User melakukan transaksi data yang valid
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu Setor Simpanan
    #When I input Nomor Rekening for Setor Simpanan Payment
    #When I input Jumlah Setoran
    #And I click button KIRIM
    #And I input password
    #Then Transaction Setor Simpan success <feature>
    #Then Transaction Receipt Save to PDF
#
    #Examples: 
      #| case | feature |
      #|    1 | Setor Simpanan |