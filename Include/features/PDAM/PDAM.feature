#Author: dillajuitaputri@gmail.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Feature PDAM
  I want to use this template for my feature file

  @tag1
  Scenario Outline: PDAM Payment
    Given Case <case>
    Given I run the application
    When I try login with existing account
    When I tap menu PDAM
    When I choose Daerah PDAM
    When I input ID Pelanggan
    And I input password
    Then Need help or go to catatan aktivitas PDAM
    #Then Transaction PDAM success <feature>
    #Then Action struk

    Examples: 
      | case | feature |
      |    1 |   PDAM  |
      
      #@tag2
  #Scenario Outline: Cari daerah PDAM dengan kode daerah
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PDAM
    #When I search Daerah PDAM
    #When I input ID Pelanggan
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    2 |
      
      #@tag3
  #Scenario Outline: Cari daerah PDAM dengan kode daerah
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PDAM
    #When I search Daerah PDAM
    #When I input ID Pelanggan
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    3 |
      
      #@tag4
  #Scenario Outline: ID Pelanggan
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PDAM
    #When I choose Daerah PDAM
    #When I input ID Pelanggan
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    4 |
      
      #@tag5
  #Scenario Outline: Salah memasukkan password transaksi
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PDAM
    #When I choose Daerah PDAM
    #When I input ID Pelanggan
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    5 |
      
      #@tag6
  #Scenario Outline: Salah memasukkan password transaksi sebanyak 3x
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PDAM
    #When I choose Daerah PDAM
    #When I input ID Pelanggan
    #And I input password
    #Then Action struk
    #Examples: 
      #| case |
      #|    6 |
      
      #@tag7
  #Scenario Outline: Session habis
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PDAM
    #When I choose Daerah PDAM
    #When I input ID Pelanggan
    #And Session over
    #Examples: 
      #| case |
      #|    7 |
      
      #@tag8
  #Scenario Outline: Koneksi terputus
    #Given Case <case>
    #Given I run the application
    #When I try login with existing account
    #When I tap menu PDAM
    #When I choose Daerah PDAM
    #When I input ID Pelanggan
    #And I input password
    #Then Action struk
#
    #Examples: 
      #| case |
      #|    8 |